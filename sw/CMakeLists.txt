cmake_minimum_required(VERSION 3.16)
include(FetchContent)

project(homeControl)
set(myLibName homeControlLib)

include_directories(external/libmodbus/include)
link_directories(external/libmodbus/lib)
set (CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_THREAD_PREFER_PTHREAD 1)

# Specify path to own LVGL config header
set(LV_CONF_PATH
    ${CMAKE_CURRENT_SOURCE_DIR}/lv_conf.h
    CACHE STRING "" FORCE)
    
set(LV_DEMO_CONF_PATH
    ${CMAKE_CURRENT_SOURCE_DIR}/lv_demo_conf.h
    CACHE STRING "" FORCE)

    
FetchContent_Declare(lvgl GIT_REPOSITORY https://github.com/lvgl/lvgl.git
                    GIT_TAG release/v8.2)

FetchContent_MakeAvailable(lvgl)
FetchContent_Declare(lv_drivers
                     GIT_REPOSITORY https://github.com/lvgl/lv_drivers.git
                     GIT_TAG release/v8.2)

FetchContent_MakeAvailable(lv_drivers)

# FetchContent_Declare(lv_demos
#                      GIT_REPOSITORY https://github.com/lvgl/lv_demos.git
#                      GIT_TAG release/v8.1)
# 
# FetchContent_MakeAvailable(lv_demos)

FetchContent_Declare(
  cJSON
  GIT_REPOSITORY https://github.com/DaveGamble/cJSON.git
  GIT_TAG        v1.7.15
)
FetchContent_MakeAvailable(cJSON)
include_directories(${cJSON_SOURCE_DIR})

FetchContent_Declare(
  magic_inum
  GIT_REPOSITORY https://github.com/Neargye/magic_enum.git
  GIT_TAG        v0.8.1
)
FetchContent_MakeAvailable(magic_inum)

# find_library(cjsonLib lvgl::lvgl)
# if(NOT cjsonLib)
#   message(FATAL_ERROR "cjsonLib library not found")
# endif()

add_subdirectory(lib)
set(HCTR_TESTING 1)

# if(${HCTR_TESTING})
    add_subdirectory(test)
# endif()
               
# target_link_libraries(homeControl PRIVATE lvgl::lvgl lvgl::drivers lvgl::demos SDL2 libmodbus.so cjson)
add_executable(homeControl main.cpp)
target_link_libraries(homeControl PRIVATE lvgl::lvgl lvgl::drivers SDL2 ${myLibName})
