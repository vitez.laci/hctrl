#ifndef MODBUSDEVICECACHE_H
#define MODBUSDEVICECACHE_H
#include <vector>
#include <cstdint>

class ModbusDeviceCache
{
public:
    ModbusDeviceCache(uint16_t startAddress, uint16_t endAddress);
    uint16_t operator[](int idx);
    uint16_t* data();
    const uint16_t normalizedStartAddress;
    const uint16_t normalizedEndAddress;
    virtual ~ModbusDeviceCache()=default;
    ModbusDeviceCache(ModbusDeviceCache&&);
    ModbusDeviceCache(const ModbusDeviceCache&)=delete;
    ModbusDeviceCache& operator=(const ModbusDeviceCache&)=delete;
private:
    uint16_t normalizeRegisterAddress(uint16_t address);
    std::vector<uint16_t> cache;
protected:
    virtual size_t getCacheSize();
};

#endif // MODBUSDEVICECACHE_H
