#ifndef MONOSTABILVALVE_H
#define MONOSTABILVALVE_H

#include "I2WayValve.h"
#include "IOutputPort.h"
class MonostabilValve:public I2WayValve
{
public:
    static const char* typeName;
    enum class DefaultState{open, closed};
    MonostabilValve(IOutputPort<bool>& port, DefaultState defaultState);
    void open() override;
    void close() override;
    DefaultState getDefaultState();
private:
    IOutputPort<bool>& port;
    const DefaultState defaultState;
};

#endif // MONOSTABILVALVE_H
