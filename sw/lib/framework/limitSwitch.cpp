#include "limitSwitch.h"

const char* LimitSwitch::typeName="LimitSwitch";

LimitSwitch::LimitSwitch(const char* name,IInputPort<bool>& port, LimitSwitch::Behavior behavior)
    :name(name),port(port),behavior(behavior)
{
}
LimitSwitch::State LimitSwitch::getState()
{
    if(Behavior::trueOnEndpoint==behavior){
        return port.read() ? State::onEndpoint : State::notOnEndpoint;
    }
    else{
        return port.read() ? State::notOnEndpoint : State::onEndpoint;
    }
}
const std::string& LimitSwitch::getName()
{
    return name;
}

