#ifndef PUMP_H
#define PUMP_H

#include "IOutputPort.h"

class Pump
{
public:
    static const char* typeName;
    Pump(IOutputPort<bool>& port);
    void Start();
    void Stop();
private:
    IOutputPort<bool>& port;
};

#endif // PUMP_H
