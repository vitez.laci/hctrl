#ifndef SIMPLEOUTPUT_H
#define SIMPLEOUTPUT_H
#include "IOutputPort.h"
class SimpleOutput
{
public:
    static const char* typeName;
    SimpleOutput(IOutputPort<bool>&port);
    void write(bool);
private:
    IOutputPort<bool>& port;
};

#endif // SIMPLEOUTPUT_H
