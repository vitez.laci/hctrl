#include "valveWithCloseLimitSwitch.h"

ValveWithCloseLimitSwitch::ValveWithCloseLimitSwitch(std::shared_ptr<I2WayValve> valve, std::shared_ptr<LimitSwitch> limitSwitch, std::shared_ptr<ValveErrorDetector> errorDetector)
:ValveWithOneLimitSwitch(valve,limitSwitch,errorDetector)
{
}

void ValveWithCloseLimitSwitch::open()
{
    valve->open();
}

void ValveWithCloseLimitSwitch::close()
{
    valve->close();
    errorDetector->triggerAction();
}
bool ValveWithCloseLimitSwitch::isClosed()
{
    return isInTerminalPosition();
}
