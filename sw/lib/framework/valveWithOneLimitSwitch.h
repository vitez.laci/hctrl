#ifndef VALVEWITHONELIMITSWITCH_H
#define VALVEWITHONELIMITSWITCH_H

#include <memory>
#include "I2WayValve.h"
#include "limitSwitch.h"
#include "valveErrorDetector.h"

class ValveWithOneLimitSwitch:public I2WayValve
{
public:
    virtual ~ValveWithOneLimitSwitch()=default;
    bool hasError();
protected:
    ValveWithOneLimitSwitch(std::shared_ptr<I2WayValve> valve,
                            std::shared_ptr<LimitSwitch> limitSwitch,
                            std::shared_ptr<ValveErrorDetector> errorDetector
                           );
    bool isInTerminalPosition();
    std::shared_ptr<I2WayValve> valve;
    std::shared_ptr<LimitSwitch> limitSwitch;
    std::shared_ptr<ValveErrorDetector> errorDetector;
};

#endif // VALVEWITHONELIMITSWITCH_H
