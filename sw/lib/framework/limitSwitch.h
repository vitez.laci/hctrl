#ifndef LIMITSWITCH_H
#define LIMITSWITCH_H

#include "IInputPort.h"
#include <string>

class LimitSwitch
{
public:
    static const char* typeName;
    enum class Behavior{trueOnEndpoint,falseOnEndpoint};
    enum class State{onEndpoint, notOnEndpoint};
    LimitSwitch(const char* name, IInputPort<bool>&port, Behavior behavior);
    virtual State getState();
    virtual const std::string& getName();
    virtual ~LimitSwitch()=default;
private:
    const std::string name;
    IInputPort<bool>& port;
    const Behavior behavior;
};

#endif // LIMITSWITCH_H
