#ifndef VALVEWITHOPENOPENLIMITSWITCH_H
#define VALVEWITHOPENOPENLIMITSWITCH_H

#include "valveWithOneLimitSwitch.h"
class ValveWithOpenLimitSwitch:public ValveWithOneLimitSwitch
{
public:
    ValveWithOpenLimitSwitch(std::shared_ptr<I2WayValve> valve,
                            std::shared_ptr<LimitSwitch> limitSwitch,
                            std::shared_ptr<ValveErrorDetector> errorDetector);
    void close() override;
    void open() override;
    bool isOpen();
};

#endif // VALVEWITHOPENOPENLIMITSWITCH_H
