#include "valveWithOneLimitSwitch.h"

using namespace std;

ValveWithOneLimitSwitch::ValveWithOneLimitSwitch(std::shared_ptr<I2WayValve> valve, std::shared_ptr<LimitSwitch> limitSwitch, std::shared_ptr<ValveErrorDetector> errorDetector)
    :valve(move(valve)),limitSwitch(limitSwitch),errorDetector(errorDetector)
{
}

bool ValveWithOneLimitSwitch::isInTerminalPosition()
{
    if(limitSwitch->getState()==LimitSwitch::State::onEndpoint){
       errorDetector->approveAction(); 
       return true;
    }
    else{
        return false;
    }
}
bool ValveWithOneLimitSwitch::hasError()
{
    return errorDetector->hasError();
}




