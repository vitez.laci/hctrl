#pragma once

class I2WayValve{
public:
    virtual ~I2WayValve()=default;
    virtual void open()=0;
    virtual void close()=0;
};
