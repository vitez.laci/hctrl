#ifndef BISTABILVALVE_H
#define BISTABILVALVE_H

#include "I2WayValve.h"
#include "IOutputPort.h"

class BistabilValve: public I2WayValve
{
public:
    BistabilValve(IOutputPort<bool>& directionOpenOutput, IOutputPort<bool>& directionCloseOutput);
    void open() override;
    void close() override;
    void stop();
private:
    IOutputPort<bool>& directionOpenOutput;
    IOutputPort<bool>& directionCloseOutput;
};

#endif // BISTABILVALVE_H
