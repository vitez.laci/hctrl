#include "pump.h"
const char* Pump::typeName="Pump";
Pump::Pump(IOutputPort<bool>& port)
:port(port)
{
}
void Pump::Start()
{
    port.write(true);
}
void Pump::Stop()
{
    port.write(false);
}


