#pragma once

template <typename T>
class IInputPort{
public:
    virtual ~IInputPort()=default;
    virtual T read()=0;
};

