#include "bistabilValve.h"
BistabilValve::BistabilValve(IOutputPort<bool>& directionOpenOutput, IOutputPort<bool>& directionCloseOutput)
:directionOpenOutput(directionOpenOutput),directionCloseOutput(directionCloseOutput)
{
}

void BistabilValve::open()
{
    directionCloseOutput.write(false);
    directionOpenOutput.write(true);
}

void BistabilValve::close()
{
    directionOpenOutput.write(false);
    directionCloseOutput.write(true);
}
void BistabilValve::stop()
{
    directionCloseOutput.write(false);
    directionOpenOutput.write(false);
}



