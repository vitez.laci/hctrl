#pragma once

template <typename T>
class IOutputPort{
public:
    virtual ~IOutputPort()=default;
    virtual void write(T value)=0;
};
