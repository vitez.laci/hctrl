#ifndef VALVEERRORDETECTOR_H
#define VALVEERRORDETECTOR_H

#include "limitSwitch.h"
#include <chrono>



class ValveErrorDetector
{
public:
    ValveErrorDetector(std::chrono::milliseconds thresholdForError);
    virtual ~ValveErrorDetector()=default;
    virtual void triggerAction();
    virtual void approveAction();
    virtual bool hasError();
private:
    const std::chrono::milliseconds thresholdForError;
    std::chrono::system_clock::time_point lastAction;
    bool isApproved;
};

#endif // VALVEERRORDETECTOR_H
