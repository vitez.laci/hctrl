#include "monostabilValve.h"

const char* MonostabilValve::typeName="MonostabilValve";
MonostabilValve::MonostabilValve(IOutputPort<bool>& port, MonostabilValve::DefaultState defaultState)
:port(port),defaultState(defaultState)
{
}
void MonostabilValve::open()
{
    port.write(true);
}
void MonostabilValve::close()
{
    port.write(false);
}
MonostabilValve::DefaultState MonostabilValve::getDefaultState()
{
    return defaultState;
}
