#ifndef VALVEWITHCLOSELIMITSWITCH_H
#define VALVEWITHCLOSELIMITSWITCH_H

#include "valveWithOneLimitSwitch.h"
class ValveWithCloseLimitSwitch:public ValveWithOneLimitSwitch
{
public:
    ValveWithCloseLimitSwitch(std::shared_ptr<I2WayValve> valve,
                            std::shared_ptr<LimitSwitch> limitSwitch,
                            std::shared_ptr<ValveErrorDetector> errorDetector);
    void close() override;
    void open() override;
    bool isClosed();
};

#endif // VALVEWITHCLOSELIMITSWITCH_H
