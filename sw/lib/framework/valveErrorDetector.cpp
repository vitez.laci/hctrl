#include "valveErrorDetector.h"

using namespace std;

ValveErrorDetector::ValveErrorDetector(chrono::milliseconds thresholdForError)
:thresholdForError(thresholdForError),lastAction(chrono::system_clock::now()), isApproved(true)
{
    
}
void ValveErrorDetector::triggerAction()
{
    isApproved=false;
    lastAction=chrono::system_clock::now();
}
void ValveErrorDetector::approveAction()
{
    isApproved=true;
}
bool ValveErrorDetector::hasError()
{
    if(!isApproved && (chrono::system_clock::now()-lastAction)>thresholdForError){
        return true;    
    }
    return false;
}


