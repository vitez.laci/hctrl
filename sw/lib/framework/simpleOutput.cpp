#include "simpleOutput.h"


const char* SimpleOutput::typeName="SimpleOutput";
SimpleOutput::SimpleOutput(IOutputPort<bool>& port)
:port(port)
{
}
void SimpleOutput::write(bool value)
{
    port.write(value);
}

