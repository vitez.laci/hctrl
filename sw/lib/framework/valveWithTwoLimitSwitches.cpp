#include "valveWithTwoLimitSwitches.h"

ValveWithTwoLimitSwitches::ValveWithTwoLimitSwitches(std::shared_ptr<I2WayValve> valve, LimitSwitch& limitSwitchForOpen, std::shared_ptr<ValveErrorDetector> errorDetectorForOpen, LimitSwitch& limitSwitchForClose, std::shared_ptr<ValveErrorDetector> errorDetectorForClose)
:valve(valve), limitSwitchForOpen(limitSwitchForOpen),errorDetectorForOpen(errorDetectorForOpen), 
limitSwitchForClose(limitSwitchForClose),errorDetectorForClose(errorDetectorForClose)
{
}
void ValveWithTwoLimitSwitches::open()
{
    valve->open();
    errorDetectorForOpen->triggerAction();
}

void ValveWithTwoLimitSwitches::close()
{
    valve->close();
    errorDetectorForClose->triggerAction();
}
bool ValveWithTwoLimitSwitches::isOpen()
{
    if(LimitSwitch::State::onEndpoint == limitSwitchForOpen.getState()){
        errorDetectorForOpen->approveAction();
        return true;
    }
    else{
        return false;
    }
    
}
bool ValveWithTwoLimitSwitches::isClosed()
{
    if(LimitSwitch::State::onEndpoint == limitSwitchForClose.getState()){
        errorDetectorForClose->approveAction();
        return true;
    }
    else{
        return false;
    }
}
bool ValveWithTwoLimitSwitches::hasError()
{
    return errorDetectorForOpen->hasError()||errorDetectorForClose->hasError();
}



