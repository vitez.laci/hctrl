#ifndef VALVEWITHTWOLIMITSWITCHES_H
#define VALVEWITHTWOLIMITSWITCHES_H
#include <memory>

#include "I2WayValve.h"
#include "limitSwitch.h"
#include "valveErrorDetector.h"

class ValveWithTwoLimitSwitches :public I2WayValve
{
public:
    ValveWithTwoLimitSwitches(std::shared_ptr<I2WayValve> valve,
                            LimitSwitch& limitSwitchForOpen,
                            std::shared_ptr<ValveErrorDetector> errorDetectorForOpen,
                            LimitSwitch& limitSwitchForClose,
                            std::shared_ptr<ValveErrorDetector> errorDetectorForClose
                             );
    void close() override;
    void open() override;
    bool isOpen();
    bool isClosed();
    bool hasError();
private:
    std::shared_ptr<I2WayValve> valve;
    LimitSwitch& limitSwitchForOpen;
    std::shared_ptr<ValveErrorDetector> errorDetectorForOpen;
    LimitSwitch& limitSwitchForClose;
    std::shared_ptr<ValveErrorDetector> errorDetectorForClose;
    
};

#endif // VALVEWITHTWOLIMITSWITCHES_H
