#include "valveWithOpenLimitSwitch.h"

ValveWithOpenLimitSwitch::ValveWithOpenLimitSwitch(std::shared_ptr<I2WayValve> valve, std::shared_ptr<LimitSwitch> limitSwitch, std::shared_ptr<ValveErrorDetector> errorDetector)
:ValveWithOneLimitSwitch(valve,limitSwitch,errorDetector)
{
}
void ValveWithOpenLimitSwitch::open()
{
    valve->open();
    errorDetector->triggerAction();
}
void ValveWithOpenLimitSwitch::close()
{
    valve->close();
}
bool ValveWithOpenLimitSwitch::isOpen()
{
    return isInTerminalPosition();
}



