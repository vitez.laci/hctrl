#ifndef COUTSERIALIZER_H
#define COUTSERIALIZER_H

#include "rawTextSerializer.h"
class CoutSerializer final:public RawTextSerializer
{
public:
    void serialize(IRawTextSrializable*) override;
};

#endif // COUTSERIALIZER_H
