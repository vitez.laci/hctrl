#ifndef RAWTEXTSERIALIZER_H
#define RAWTEXTSERIALIZER_H

#include "../engine/ISerializer.h"
#include "../engine/IRawTextSerializable.h"
#include "../engine/threadSafeLogItemStore.h"

class RawTextSerializer : public ISerializer<IRawTextSrializable>
{
public:
    virtual ~RawTextSerializer()=default;
};

#endif // RAWTEXTSERIALIZER_H
