#ifndef FILEANDRUNTIMEINFO_H
#define FILEANDRUNTIMEINFO_H
#include <cstdint>
struct FileAndRuntimeInfo
{
    const char* threadName;
    const char* fileName;
    const uint32_t lineNo;
    const char* callstack;
    FileAndRuntimeInfo(const char* threadName,
                        const char* fileName,
                        const uint32_t lineNo,
                        const char* callstack);
};

#endif // FILEANDRUNTIMEINFO_H
