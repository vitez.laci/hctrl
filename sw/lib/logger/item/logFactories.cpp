#include "logFactories.h"

using namespace std;

std::shared_ptr<LogEvent> LogFactories::LogEventFactory::CreateLogEvent(const char* source, const char* eventType, const char* message)
{
    shared_ptr<LogEvent>event(new LogEvent());
    event->setSourceName(source);
    event->setTimestamp(chrono::system_clock::now());
    event->setMessage(message);
    event->setEventType(eventType);
    return event;
}

