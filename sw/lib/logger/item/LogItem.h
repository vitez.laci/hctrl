#pragma once
#include "../engine/IRawTextSerializable.h"
#include <chrono>

class LogItem: public IRawTextSrializable{
public:
    LogItem()=default;
    virtual ~LogItem()=default;
    LogItem(const LogItem& other);
    LogItem(LogItem&& other);
    LogItem& operator=(const LogItem&);
    LogItem& operator=(LogItem&&);
    void setSourceName(const char*);
    void setTimestamp(std::chrono::system_clock::time_point);
    void setMessage(std::string&);
    void setMessage(const char*);
    virtual std::string toRawText() override;
protected:
    const char* sourceName=nullptr;
    std::chrono::system_clock::time_point timestamp;
    std::string message;
    virtual std::stringstream getRawRextStream() override;
};
