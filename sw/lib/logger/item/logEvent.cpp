#include "logEvent.h"
#include <sstream>

using namespace std;


LogEvent::LogEvent(const LogEvent& other)
:eventType(other.eventType)
{
}
LogEvent::LogEvent(LogEvent && other)
:eventType(std::move(other.eventType))
{
}
LogEvent & LogEvent::operator=(const LogEvent& o)
{
    eventType=o.eventType;
    return *this;
}
LogEvent & LogEvent::operator=(LogEvent && o)
{
    eventType=std::move(o.eventType);
    return *this;
}





void LogEvent::setEventType(const char* eventType)
{
    this->eventType=eventType;
}
std::stringstream LogEvent::getRawRextStream()
{
    auto ss=LogItem::getRawRextStream();
    ss<<"Event Type: "<<eventType<<endl<<endl;
    return ss;
}

