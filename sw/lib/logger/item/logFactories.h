#pragma once
#include "logEvent.h"
#include <memory>
namespace LogFactories{
    namespace LogEventFactory
    {
        std::shared_ptr<LogEvent> CreateLogEvent(const char* source,const char* eventType,const char* message);
    };
};

