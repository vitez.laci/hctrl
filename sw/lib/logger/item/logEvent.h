#ifndef LOGEVENT_H
#define LOGEVENT_H

#include <string>
#include "LogItem.h"

class LogEvent :public LogItem
{
public:
    LogEvent()=default;
    virtual ~LogEvent()=default;
    LogEvent(const LogEvent& other);
    LogEvent(LogEvent&& other);
    LogEvent& operator=(const LogEvent&);
    LogEvent& operator=(LogEvent&&);
    void setEventType(const char*);
protected:
    virtual std::stringstream getRawRextStream() override;
private:
    std::string eventType;
};

#endif // LOGEVENT_H
