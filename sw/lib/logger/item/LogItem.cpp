#include "LogItem.h"
#include <sstream>
#include <iostream>
#include <ctime>
#include <iomanip>

using namespace std;

LogItem::LogItem(const LogItem& other)
:sourceName(other.sourceName),timestamp(other.timestamp),message(other.message)
{
}
LogItem::LogItem(LogItem && other)
:sourceName(other.sourceName), timestamp(std::move(other.timestamp)), message(std::move(other.message))
{
    other.sourceName=nullptr;
}


LogItem & LogItem::operator=(const LogItem& other)
{
    sourceName=other.sourceName;
    timestamp=other.timestamp;
    message=other.message;
    return *this;
}
LogItem & LogItem::operator=(LogItem && other)
{
    if(this!=&other){   
        sourceName=other.sourceName;
        timestamp=std::move(other.timestamp);
        message=std::move(other.message);
    }
    return *this;
}
void LogItem::setMessage(std::string& message)
{
    this->message=message;
}
void LogItem::setMessage(const char* message)
{
    this->message=message;
}

void LogItem::setSourceName(const char* sourceName)
{
    this->sourceName=sourceName;
}
void LogItem::setTimestamp(std::chrono::system_clock::time_point timestamp)
{
    this->timestamp=timestamp;
}
std::string LogItem::toRawText()
{
    return getRawRextStream().str();
}
std::stringstream LogItem::getRawRextStream()
{
    stringstream ss;
    const std::time_t t_c=chrono::system_clock::to_time_t(timestamp);
    
    ss<<"SourceName: "<<sourceName<<endl<<"timestamp: "<<put_time(localtime(&t_c),"%F %T")<<endl<<"message: "<<message<<endl;
    return ss;
}









