#ifndef LOGSCHEDULER_H
#define LOGSCHEDULER_H

#include "ILogger.h"
#include <set>
#include <chrono>
#include <atomic>
#include <thread>
#include <memory>
#include <mutex>

class LogScheduler
{
public:
    LogScheduler(std::chrono::microseconds sleepInterval);
    ~LogScheduler();
    void registerLoggerTarget(ILogger*);
    void removeLoggerTarget(ILogger*);
private:
    std::mutex loggersLock;
    std::chrono::microseconds sleepInterval;
    std::set<ILogger*> loggers;
    std::atomic<bool> exitRequest=false;
    std::thread workerThread;
    void logFn();
    
};

#endif // LOGSCHEDULER_H
