#pragma once

#include <cstdint>

struct ILogger{
    virtual ~ILogger()=default;
    virtual void persistLogItems(uint32_t count)=0;
    virtual void persistAllItem()=0;
};
