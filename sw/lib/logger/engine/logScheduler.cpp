#include "logScheduler.h"
#include <functional>
#include <iostream>

using namespace std;

LogScheduler::LogScheduler(std::chrono::microseconds sleepInterval)
:sleepInterval(sleepInterval),workerThread(std::bind(&LogScheduler::logFn,this))
{
    cout<<"Log scheduler started. sleepInterval is "<<sleepInterval.count()<<" us"<<endl;
}
LogScheduler::~LogScheduler()
{
    cout<<"Stopping"<<endl;
    exitRequest=true;
    workerThread.join();
    for(auto logger : loggers){
        logger->persistAllItem();
    }
    cout<<"Log scheduler stopped"<<endl;
}

void LogScheduler::logFn()
{
    while(!exitRequest){
        {
            volatile std::unique_lock<std::mutex>(loggersLock);
            for(auto logger : loggers){
                logger->persistAllItem();
            }
        }
        this_thread::sleep_for(sleepInterval);
    }
    
}
void LogScheduler::registerLoggerTarget(ILogger* newLogger)
{
    volatile std::unique_lock<std::mutex>(loggersLock);
    if(!loggers.contains(newLogger)){
        loggers.emplace(newLogger);
    }
}
void LogScheduler::removeLoggerTarget(ILogger* removable)
{
    volatile std::unique_lock<std::mutex>(loggersLock);
    loggers.erase(removable);
}




