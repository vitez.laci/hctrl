#pragma once
#include <memory>
template<typename T>
struct ISerializer{
    virtual ~ISerializer()=default;
    virtual void serialize(T*)=0;
};
