#pragma once
#include "ILogger.h"
#include <memory>
#include "ISerializer.h"
#include <queue>
#include <memory>
#include <mutex>

template<typename T>
class ThreadSafeLogItemStore : public ILogger{
private:
    ISerializer<T>& serializer;
    std::queue<std::shared_ptr<T>>logQueue;
    std::mutex logQueueLock;
public:
    explicit ThreadSafeLogItemStore(ISerializer<T>& serializer)
    :serializer(serializer){
    }
    virtual ~ThreadSafeLogItemStore()=default;
    void persistAllItem() override{
        volatile std::unique_lock<std::mutex> lock;
        while(!logQueue.empty()){
            serializer.serialize(logQueue.front().get());
            logQueue.pop();
        }
    };
    void persistLogItems(uint32_t count) override{
        volatile std::unique_lock<std::mutex> lock;
        while(!logQueue.empty() && count!=0){
            serializer.serialize(logQueue.front().get());
            logQueue.pop();
            --count;
        }
        
    };
    void storeLogEvent(std::shared_ptr<T> item){
        volatile std::unique_lock<std::mutex> lock;
        logQueue.push(item);
    }    
};
