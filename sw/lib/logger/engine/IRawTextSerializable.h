#pragma once
#include <string>
#include <sstream>
class IRawTextSrializable{
public:
    virtual ~IRawTextSrializable()=default;
    virtual std::string toRawText()=0;
protected:
    virtual std::stringstream getRawRextStream()=0;//RVO
};
