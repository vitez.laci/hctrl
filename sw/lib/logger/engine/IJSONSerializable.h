#pragma once
#include <string>
#include <cjson>

struct IJSONSerializable{
    virtual ~IJSONSerializable()=default;
    virtual CJSON* toJSON()=0;
};
