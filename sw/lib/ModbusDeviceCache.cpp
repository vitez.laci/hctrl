#include "ModbusDeviceCache.h"

ModbusDeviceCache::ModbusDeviceCache(uint16_t startAddress, uint16_t endAddress)
:normalizedStartAddress(normalizeRegisterAddress(startAddress)), normalizedEndAddress(normalizeRegisterAddress(endAddress)),
cache((normalizedEndAddress-normalizedStartAddress)+1)
{
}
ModbusDeviceCache::ModbusDeviceCache(ModbusDeviceCache && other)
:normalizedStartAddress(other.normalizedStartAddress),normalizedEndAddress(other.normalizedEndAddress),cache(std::move(other.cache))
{
}


uint16_t ModbusDeviceCache::normalizeRegisterAddress(uint16_t address)
{
        if(address>40000)
            return address-40001;
        else
            return address;
}

uint16_t ModbusDeviceCache::operator[](int idx)
{
    return cache[idx];
}
uint16_t * ModbusDeviceCache::data()
{
    return cache.data();
}
size_t ModbusDeviceCache::getCacheSize()
{
    return cache.size();
}




