#include "modbusDevice.h"
#include <sstream>
#include <functional>
using namespace std;

ModbusDevice::ModbusDevice(std::shared_ptr<ModbusController> controller, const char* name, uint16_t address,std::map<const char*,shared_ptr<ModbusDeviceCache>>&& cache)
:controller(controller),name(name),address(address),cache(std::move(cache))
{
}
const std::string & ModbusDevice::getName()
{
    return name;
}

ModbusDeviceCache & ModbusDevice::getCache(const char* cacheId)
{
    if(!cache.contains(cacheId)){
        stringstream ss;
        ss<<"Invalid cache <"<<cacheId<<"> for modbus device <"<<name<<">";
        throw runtime_error(ss.str());
    }
    return *cache.at(cacheId);
}
void ModbusDevice::updateCache()
{
    for(auto it: cache){
        controller->readRegisters(address,it.second->normalizedStartAddress,it.second->normalizedEndAddress,it.second->data());
    }
}

