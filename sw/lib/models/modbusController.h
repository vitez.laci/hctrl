#ifndef MODBUSCONTROLLER_H
#define MODBUSCONTROLLER_H

#include <cstdint>
#include <string>
#include <modbus/modbus.h>
#include <vector>

class ModbusController
{
public:
    static const char* typeName;
    const std::string port;
    const uint16_t baudrate;
    const char parity;
    const uint8_t databit;
    const float stopbit;
    const std::string comment;
    ModbusController(
        const char* port,
        uint16_t baudrate,
        char parity,
        uint8_t databit,
        float stopbit,
        const char* comment
    );
    virtual ~ModbusController();
    virtual void readRegisters(uint16_t deviceAddress,uint16_t startAddress, uint16_t endAddress, uint16_t* target);
protected:
    ModbusController();   
private:
    modbus_t *mb=nullptr;
};

#endif // MODBUSCONTROLLER_H
