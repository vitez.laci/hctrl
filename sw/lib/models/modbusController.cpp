#include "modbusController.h"
#include <sstream>
#include "../logManager.h"
#include "../logger/item/logFactories.h"

using namespace std;
const char* ModbusController::typeName="ModbusController";

ModbusController::ModbusController(const char* port, uint16_t baudrate, char parity, uint8_t databit, float stopbit, const char* comment)
:port(port),baudrate(baudrate),parity(parity),databit(databit),stopbit(stopbit),comment(comment)
{
    mb = modbus_new_rtu(port,baudrate,parity,databit,stopbit);
    if(mb==NULL){
        stringstream ss;
        ss<<"Unable to open modbus device on "<<endl<<
        "-port:"<<port<<endl<<
        "-baudrate:"<<baudrate<<endl<<
        "-parity:"<<parity<<endl<<
        "-databit:"<<databit<<endl<<
        "-stopbit:"<<stopbit<<endl<<
        "-comment:"<<comment<<endl;
        throw runtime_error(ss.str());
    };
}
ModbusController::ModbusController()
:port(""),baudrate(0),parity('X'),databit(0),stopbit(0),comment("")
{
}

ModbusController::~ModbusController()
{
    if(mb){
        modbus_close(mb);
        modbus_free(mb);
    }
}
void ModbusController::readRegisters(uint16_t deviceAddress, uint16_t startAddress, uint16_t endAddress, uint16_t* target)
{
    int rc = modbus_set_slave(mb, deviceAddress);
    if (rc == -1) {
        throw runtime_error("invalid slave Id");
    }
    if (modbus_connect(mb) == -1) {
        stringstream ss;
        ss<<"Connection failed: "<<modbus_strerror(errno);
        throw runtime_error(ss.str());
    }
    modbus_read_registers(mb, startAddress, endAddress, target);//error handling!!!
    modbus_close(mb);
}


        
//     uint16_t tab_reg[32];
// 
// 
// 
//     int rc = modbus_set_slave(mb, 1);
//     if (rc == -1) {
//         fprintf(stderr, "Invalid slave ID\n");
//         modbus_free(mb);
//         return -1;
//     }
// 
//     modbus_get_response_timeout(mb,&respTimeoutS,&respTimeoutuS);
//     if (modbus_connect(mb) == -1) {
//         fprintf(stderr, "Connection failed: %s\n", modbus_strerror(errno));
//         modbus_free(mb);
//         return -1;
//     }
//     modbus_set_response_timeout(mb,0,500000);
// for(int i=0;i<10;++i){
//     /* Read 5 registers from the address 0 */
//     modbus_read_registers(mb, 0, 11, tab_reg);
//     float room_temp=float(tab_reg[8])/10.0;
//     float set_temp=float(tab_reg[3])/10.0;
//     cout<<"temp: "<<room_temp<<"("<<set_temp<<")"<<endl;
//     this_thread::sleep_for(1s);
// }
    


