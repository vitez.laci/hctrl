#pragma once
struct IThermostat{
    virtual ~IThermostat()=default;
    enum class State{On,Off};
    enum class FanSpeedSetting {Auto,High,Mid,Low};
    enum class OperatonMode{Cooling,Heating,Ventillation};
    enum class LockState{Unlocked, Locked};
    enum class OperatingFanSpeed{Low,Mid,High,Off};
    virtual State getState()=0;
    virtual void setState()=0;
    virtual FanSpeedSetting getFanSpeedSetting()=0;
    virtual void setFanSpeedSetting(FanSpeedSetting)=0;
    virtual OperatonMode getOperationMode()=0;
    virtual void setOperationMode(OperatonMode)=0;
    virtual float getTemperaturePoint()=0;
    virtual void setTemperaturePoint(float)=0;
    virtual LockState getLockState()=0;
    virtual void setLockState(LockState)=0;
    virtual float getRoomTemerature()=0;
    virtual bool isCoolingValveOn()=0;
    virtual bool isHeatingValveOn()=0;
    virtual OperatingFanSpeed getFanSpeed()=0;
};
