#ifndef BAC_1000ALN_H
#define BAC_1000ALN_H

#include "IThermostat.h"
#include "modbusDevice.h"
#include <vector>

class BAC_1000ALN :public IThermostat, public ModbusDevice
{
public:
    BAC_1000ALN(std::shared_ptr<ModbusController> controller,const char* name, uint16_t address);
    static const char* typeName;
    BAC_1000ALN();
    virtual State getState() override;
    virtual void setState() override;
    virtual FanSpeedSetting getFanSpeedSetting() override;
    virtual void setFanSpeedSetting(FanSpeedSetting) override;
    virtual OperatonMode getOperationMode() override;
    virtual void setOperationMode(OperatonMode) override;
    virtual float getTemperaturePoint() override;
    virtual void setTemperaturePoint(float) override;
    virtual LockState getLockState() override;
    virtual void setLockState(LockState) override;
    virtual float getRoomTemerature() override;
    virtual bool isCoolingValveOn() override;
    virtual bool isHeatingValveOn() override;
    virtual OperatingFanSpeed getFanSpeed() override;
};

#endif // BAC_1000ALN_H
