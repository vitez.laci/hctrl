#ifndef MIDEAWIREDCONTROLLER_H
#define MIDEAWIREDCONTROLLER_H

#include "modbusDevice.h"
class MideaWiredController :public ModbusDevice
{
public:
    MideaWiredController(std::shared_ptr<ModbusController> controller,const char* name, uint16_t address);
};

#endif // MIDEAWIREDCONTROLLER_H
