#include "CWT_MB308K_OutputPort.h"
#include "../models/CWT_MB308K.h"

CWT_MB308K_OutputPort::CWT_MB308K_OutputPort(CWT_MB308K* device, uint16_t registerAddress)
:device(device),registerAddress(registerAddress)
{
}
void CWT_MB308K_OutputPort::write(bool value)
{
    device->writeOutput(registerAddress,value);
}

