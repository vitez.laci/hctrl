#ifndef CWT_MB308K_InputPort_H
#define CWT_MB308K_InputPort_H

#include "../framework/IInputPort.h"
#include <cstdint>

class CWT_MB308K;
class CWT_MB308K_InputPort: public IInputPort<bool>
{
public:
    CWT_MB308K_InputPort(CWT_MB308K*, uint16_t registerAddress);
    bool read() override;
private:
    CWT_MB308K* device;
    uint16_t registerAddress;
};

#endif // CWT_MB308K_InputPort_H
