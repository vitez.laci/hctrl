#include "CWT_MB308K.h"

using namespace std;

const char* CWT_MB308K::typeName="CWT_MB308K";
CWT_MB308K::CWT_MB308K(std::shared_ptr<ModbusController> controller, const char* name, uint16_t address)
:ModbusDevice(controller,name,address,{})
{
    for(int i=0;i< inputs.size();++i){
        inputs[i]=make_unique<CWT_MB308K_InputPort>(this,i);
    }
    for(int i=0;i< outputs.size();++i){
        outputs[i]=make_unique<CWT_MB308K_OutputPort>(this,i);
    }
}
uint16_t CWT_MB308K::readInput(uint16_t portNo)
{
    return 0;
}
void CWT_MB308K::writeOutput(uint16_t portNo, uint16_t value)
{
}
IInputPort<bool> & CWT_MB308K::getInputPort(uint16_t inputNoFrom0){
    return *inputs.at(inputNoFrom0);
}
IOutputPort<bool> & CWT_MB308K::getOutputPort(uint16_t outputNoFrom0){
    return *outputs.at(outputNoFrom0);
}

