#include "CWT_MB308K_InputPort.h"
#include "CWT_MB308K.h"

CWT_MB308K_InputPort::CWT_MB308K_InputPort(CWT_MB308K* device, uint16_t registerAddress)
:device(device),registerAddress(registerAddress)
{
}

bool CWT_MB308K_InputPort::read()
{
    return device->readInput(registerAddress);
}


