#ifndef CWT_MB308K_OUTPUTPORT_H
#define CWT_MB308K_OUTPUTPORT_H

#include "../framework/IOutputPort.h"
#include <cstdint>
class CWT_MB308K;
class CWT_MB308K_OutputPort : public IOutputPort<bool>
{
public:
    CWT_MB308K_OutputPort(CWT_MB308K*, uint16_t registerAddress);
    void write(bool value) override;
private:
    CWT_MB308K* device;
    uint16_t registerAddress;
};

#endif // CWT_MB308K_OUTPUTPORT_H
