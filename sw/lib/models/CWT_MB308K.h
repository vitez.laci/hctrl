#ifndef CWT_MB308K_H
#define CWT_MB308K_H

#include "IIOHandler.h"
#include "modbusDevice.h"
#include <memory>
#include "CWT_MB308K_InputPort.h"
#include "CWT_MB308K_OutputPort.h"
#include <array>

class CWT_MB308K:public IIOHandler, public ModbusDevice
{
public:
    virtual ~CWT_MB308K()=default;
    static const char* typeName;
    CWT_MB308K(std::shared_ptr<ModbusController> controller,const char* name, uint16_t address);
    virtual uint16_t readInput(uint16_t portNo);
    virtual void writeOutput(uint16_t portNo, uint16_t value);
    virtual IInputPort<bool>& getInputPort(uint16_t inputNoFrom0)override;
    virtual IOutputPort<bool>& getOutputPort(uint16_t outputNoFrom0)override;
private:
    std::array<std::unique_ptr<CWT_MB308K_InputPort>,16> inputs;
    std::array<std::unique_ptr<CWT_MB308K_OutputPort>,12> outputs;
};

#endif // CWT_MB308K_H
