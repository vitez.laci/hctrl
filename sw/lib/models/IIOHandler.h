#pragma once
#include <cstdint>
#include "../framework/IInputPort.h"
#include "../framework/IOutputPort.h"

struct IIOHandler {
    virtual ~IIOHandler()=default;
    virtual IInputPort<bool>& getInputPort(uint16_t inputNoFrom0)=0;
    virtual IOutputPort<bool>& getOutputPort(uint16_t outputNoFrom0)=0;
};
