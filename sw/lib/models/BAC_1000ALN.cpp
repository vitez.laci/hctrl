#include "BAC_1000ALN.h"
#include <iostream>
#include "../ModbusDeviceCache.h"

using namespace std;
const char* BAC_1000ALN::typeName="BAC_1000ALN";
BAC_1000ALN::BAC_1000ALN(std::shared_ptr<ModbusController> controller,const char* name, uint16_t address)
    :ModbusDevice(controller,name,address,{{"A",make_shared<ModbusDeviceCache>(40001,40011)}})
{
}
IThermostat::State BAC_1000ALN::getState()
{
    return State::Off;
}
void BAC_1000ALN::setState()
{
}
IThermostat::FanSpeedSetting BAC_1000ALN::getFanSpeedSetting()
{
    return FanSpeedSetting::Mid;
}
void BAC_1000ALN::setFanSpeedSetting(IThermostat::FanSpeedSetting)
{
}

IThermostat::OperatonMode BAC_1000ALN::getOperationMode()
{
    return OperatonMode::Cooling;
}

void BAC_1000ALN::setOperationMode(IThermostat::OperatonMode)
{
}

float BAC_1000ALN::getTemperaturePoint()
{
    return float(getCache("A")[3])/10.0;
}
void BAC_1000ALN::setTemperaturePoint(float)
{
}
IThermostat::LockState BAC_1000ALN::getLockState()
{
    return LockState::Locked;
}
void BAC_1000ALN::setLockState(IThermostat::LockState)
{
}
float BAC_1000ALN::getRoomTemerature()
{
    return 0;
}
bool BAC_1000ALN::isCoolingValveOn()
{
    return false;
}
bool BAC_1000ALN::isHeatingValveOn()
{
    return false;
}
IThermostat::OperatingFanSpeed BAC_1000ALN::getFanSpeed()
{
    return OperatingFanSpeed::Off;
}
