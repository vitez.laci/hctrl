#ifndef MODBUSDEVICE_H
#define MODBUSDEVICE_H
#include "modbusController.h"
#include <memory>
#include <string>
#include <map>
#include "../ModbusDeviceCache.h"

class ModbusDevice
{
public:
    ModbusDevice (std::shared_ptr<ModbusController> controller,const char* name, uint16_t address,std::map<const char*,std::shared_ptr<ModbusDeviceCache>>&& cache);
    virtual ~ModbusDevice()=default;
    const std::string& getName();
    ModbusDeviceCache&getCache(const char* cacheId);
    virtual void updateCache();
private:
    std::shared_ptr<ModbusController> controller;
    const std::string name;
    const uint16_t address;
    std::map<const char*,std::shared_ptr<ModbusDeviceCache>> cache;
};

#endif // MODBUSDEVICE_H
