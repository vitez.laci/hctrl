#include "logManager.h"
#include <chrono>
using namespace std;

LogManager LogManager::onlyInstance;
ThreadSafeLogItemStore<IRawTextSrializable> & LogManager::getCoutlogger()
{
    return onlyInstance.coutLogItemStore;
}

LogManager::LogManager()
:logScheduler(100000us),coutLogItemStore(coutSerializer)
{
    logScheduler.registerLoggerTarget(&coutLogItemStore);
}
LogManager::~LogManager()
{
    logScheduler.removeLoggerTarget(&coutLogItemStore);
}

