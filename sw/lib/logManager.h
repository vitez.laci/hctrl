#ifndef LOGMANAGER_H
#define LOGMANAGER_H
#include "logger/serializer/coutSerializer.h"
#include "logger/engine/logScheduler.h"
#include "logger/engine/threadSafeLogItemStore.h"

class LogManager final
{
public:
    static ThreadSafeLogItemStore<IRawTextSrializable>& getCoutlogger();
private:
    LogManager();
    ~LogManager();
    static LogManager onlyInstance;
    LogScheduler logScheduler;
    CoutSerializer coutSerializer;
    ThreadSafeLogItemStore<IRawTextSrializable> coutLogItemStore;
    
};

#endif // LOGMANAGER_H
