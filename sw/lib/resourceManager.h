#ifndef RESOURCEMANAGER_H
#define RESOURCEMANAGER_H
#include <map>
#include <memory>
#include "models/modbusController.h"
#include "models/IThermostat.h"
#include "models/IIOHandler.h"
#include "framework/limitSwitch.h"
#include "framework/monostabilValve.h"
#include "framework/pump.h"
#include "framework/simpleOutput.h"
#include "framework/valveWithOpenLimitSwitch.h"
#include "models/MideaWiredController.h"
#include <functional>
#include <sstream>
#include <magic_enum.hpp>
#include <chrono>
#include <vector>

using std::operator""ms;
class ResourceManager
{
public:
    enum class LimitSwitchRole {DisinfectionBypassValveFeedback,LivingRoomCoolingCircleFeedback,KitchenCoolingCircleFeedback,
        OfficeCoolingCircleFeedback,BedRoomCoolingCircleFeedback,NRoomCoolingCircleFeedback,SRoomCoolingCircleFeedback
    };
    enum class MonostabilValveRole {DisinfectionBypassValveControl,LivingRoomAndKitchenCoolingCircleControl,
        OfficeCoolingCircleControl,BedRoomCoolingCircleControl,NRoomCoolingCircleControl,SRoomCoolingCircleControl
    };
    enum class PumpRole {WarmWaterCirculation
    };
    enum class SimpleOutputRole {SimulatedThermostatOutput
    };
    enum class ValveWithOpenLimitSwitchRole{DisinfectionBypassValve, LivingRoomCoolingCircle,KitchenCoolingCircle,
        OfficeCoolingCircle,BedRoomCoolingCircle,NRoomCoolingCircle,SRoomCoolingCircle};
    static ResourceManager& I();
    virtual ~ResourceManager()=default;
    virtual void registerModbusController(const char*, std::shared_ptr<ModbusController>);
    virtual std::shared_ptr<ModbusController> getModbusController(const char*);
    virtual void registerThermostat(const char*, std::shared_ptr<IThermostat>);
    virtual std::shared_ptr<IThermostat> getThermostat(const char*);
    virtual void registerIOHandler(const char*, std::shared_ptr<IIOHandler>);
    virtual std::shared_ptr<IIOHandler> getIOHandler(const char*);
    virtual void registerLimitSwitch(LimitSwitchRole, std::shared_ptr<LimitSwitch>);
    virtual std::shared_ptr<LimitSwitch> getLimitSwitch(LimitSwitchRole);
    virtual void registerMonostabilValve(MonostabilValveRole, std::shared_ptr<MonostabilValve>);
    virtual std::shared_ptr<MonostabilValve> getMonostabilValve(MonostabilValveRole);
    virtual void registerPump(PumpRole, std::shared_ptr<Pump>);
    virtual std::shared_ptr<Pump> getPump(PumpRole);
    virtual void registerSimpleOutput(SimpleOutputRole, std::shared_ptr<SimpleOutput>);
    virtual std::shared_ptr<SimpleOutput> getSimpleOutput(SimpleOutputRole);
    virtual void registerValveWithOpenLimitSwitch(ValveWithOpenLimitSwitchRole, std::shared_ptr<ValveWithOpenLimitSwitch>);
    virtual std::shared_ptr<ValveWithOpenLimitSwitch> getValveWithOpenLimitSwitch(ValveWithOpenLimitSwitchRole);
    virtual void registerMideaController(std::shared_ptr<MideaWiredController> controller);
    virtual std::shared_ptr<MideaWiredController> getMideaController();
    virtual void setMainLoopPeriod(std::chrono::milliseconds);
    virtual std::chrono::milliseconds getMainLoopPeriod();
    virtual void registerAutoUpdatebleModbusDevice(std::shared_ptr<ModbusDevice>);
    void updateModbusDeviceCaches();
    
protected:
    static std::function< ResourceManager&()>instanceCreator;
    ResourceManager()=default;
private:
    std::map<std::string,std::shared_ptr<ModbusController>> modbusControllers;
    std::map<std::string,std::shared_ptr<IThermostat>> thermostats;
    std::map<std::string,std::shared_ptr<IIOHandler>> ioHandlers;
    std::map<LimitSwitchRole,std::shared_ptr<LimitSwitch>> limitSwitches;
    std::map<MonostabilValveRole,std::shared_ptr<MonostabilValve>> monostabilValves;
    std::map<PumpRole,std::shared_ptr<Pump>> pumps;
    std::map<SimpleOutputRole,std::shared_ptr<SimpleOutput>> simpleOutputs;
    std::map<ValveWithOpenLimitSwitchRole,std::shared_ptr<ValveWithOpenLimitSwitch>> valvesWithOpenLimitSwitch;
    std::shared_ptr<MideaWiredController> mideaController=nullptr;
    std::chrono::milliseconds mainLoopPeriod=100ms;
    std::vector<std::shared_ptr<ModbusDevice>> autoUpdatableModbusDevice;
    template <typename Tkey,typename Titem>
    void registerEnumWithException(std::map<Tkey,std::shared_ptr<Titem>>& m, Tkey k, std::shared_ptr<Titem> v, const char* excBaseText){
        if(m.contains(k)){
            std::stringstream ss;
            ss<<excBaseText<< magic_enum::enum_name(k);
            throw std::runtime_error(ss.str());
        }
        m[k]=v;
    }
    template<typename Tkey,typename Titem>
    std::shared_ptr<Titem> getEnumKeyWithException(std::map<Tkey,std::shared_ptr<Titem>>& m, Tkey k, const char* excBaseText){
        if(!m.contains(k)){
            std::stringstream ss;
            ss<<"Unable to find <"<<magic_enum::enum_name(k)<<"> in" <<excBaseText;
            throw std::runtime_error(ss.str());
        }
        return m.at(k);
    }
    
    static ResourceManager& createSingletonInstance();
};

#endif // RESOURCEMANAGER_H
