
file(GLOB srcs "./*.h" "./*.cpp" "./*.c"
                "./settings/*.cpp" "./settings/*.h"
                "./settings/parsers/*.cpp" "./settings/parsers/*.h")
add_subdirectory(framework)
add_subdirectory(logger)
add_subdirectory(models)
add_library(${myLibName} ${srcs})
target_link_libraries(${myLibName} models framework hctrLogger cjson magic_enum::magic_enum libmodbus.so)
target_include_directories(${myLibName} INTERFACE .)
