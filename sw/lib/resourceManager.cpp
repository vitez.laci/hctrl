#include "resourceManager.h"
#include <exception>
#include <sstream>
#include <magic_enum.hpp>
#include <functional>

using namespace std;

std::function< ResourceManager&()> ResourceManager::instanceCreator=ResourceManager::createSingletonInstance;

ResourceManager & ResourceManager::I()
{
    return instanceCreator();
}
std::shared_ptr<ModbusController> ResourceManager::getModbusController(const char* name)
{
    if(!modbusControllers.contains(name)){
        std::stringstream ss;
        ss<<"Unable to find <"<<name<<"> in" <<"modbusControllers role";
        throw std::runtime_error(ss.str());
    }
    return modbusControllers.at(name);
}

void ResourceManager::registerModbusController(const char* name,std::shared_ptr<ModbusController> controller)
{
    if(modbusControllers.contains(name)){
        stringstream ss;
        ss<<"Duplication of controller: "<<name;
        throw runtime_error(ss.str());
    }
    modbusControllers[name]=controller;
}
ResourceManager & ResourceManager::createSingletonInstance()
{
    static ResourceManager onlyInstance;
    return onlyInstance;
}
void ResourceManager::registerThermostat(const char* name, std::shared_ptr<IThermostat> device)
{
    if(thermostats.contains(name)){
        stringstream ss;
        ss<<"Duplication of thermostat: "<<name;
        throw runtime_error(ss.str());
    }
    thermostats[name]=device;
}
std::shared_ptr<IThermostat> ResourceManager::getThermostat(const char* name)
{
    if(!thermostats.contains(name)){
        std::stringstream ss;
        ss<<"Unable to find <"<<name<<"> in" <<"Thermostat role";
        throw std::runtime_error(ss.str());
    }
    return thermostats.at(name);
}
void ResourceManager::registerIOHandler(const char* name, std::shared_ptr<IIOHandler> exp)
{
    if(ioHandlers.contains(name)){
        stringstream ss;
        ss<<"Duplication of ioExpander: "<<name;
        throw runtime_error(ss.str());
    }
    ioHandlers[name]=exp;
}

std::shared_ptr<IIOHandler> ResourceManager::getIOHandler(const char* name)
{
    if(!ioHandlers.contains(name)){
        std::stringstream ss;
        ss<<"Unable to find <"<<name<<"> in" <<"IOHandler role";
        throw std::runtime_error(ss.str());
    }
    return ioHandlers.at(name);
}

void ResourceManager::registerLimitSwitch(ResourceManager::LimitSwitchRole role, std::shared_ptr<LimitSwitch> instance)
{
    registerEnumWithException(limitSwitches,role,instance,"Duplication of limitSwitch: ");
}

std::shared_ptr<LimitSwitch> ResourceManager::getLimitSwitch(ResourceManager::LimitSwitchRole role)
{
    return getEnumKeyWithException(limitSwitches,role,"LimitSwitch role");
}



void ResourceManager::registerMonostabilValve(ResourceManager::MonostabilValveRole role, std::shared_ptr<MonostabilValve>instance)
{
    registerEnumWithException(monostabilValves,role,instance,"Duplication of monostabil valve: ");
}
std::shared_ptr<MonostabilValve> ResourceManager::getMonostabilValve(ResourceManager::MonostabilValveRole role)
{
    return getEnumKeyWithException(monostabilValves,role,"MonostabilValve role");
}
void ResourceManager::registerPump(ResourceManager::PumpRole role, std::shared_ptr<Pump>instance)
{
    registerEnumWithException(pumps,role,instance,"Duplication of pump: ");
}

std::shared_ptr<Pump> ResourceManager::getPump(ResourceManager::PumpRole role)
{
    return getEnumKeyWithException(pumps,role,"pump role");
}
void ResourceManager::registerSimpleOutput(ResourceManager::SimpleOutputRole role, std::shared_ptr<SimpleOutput> instance)
{
    registerEnumWithException(simpleOutputs,role,instance,"Duplication of simple output: ");
}
std::shared_ptr<SimpleOutput> ResourceManager::getSimpleOutput(ResourceManager::SimpleOutputRole role)
{
    return getEnumKeyWithException(simpleOutputs,role,"simpleOutputRol role");
}

void ResourceManager::registerValveWithOpenLimitSwitch(ResourceManager::ValveWithOpenLimitSwitchRole role, std::shared_ptr<ValveWithOpenLimitSwitch> instance)
{
    registerEnumWithException(valvesWithOpenLimitSwitch,role,instance,"Duplication of valve with open limit switch: ");
}

std::shared_ptr<ValveWithOpenLimitSwitch> ResourceManager::getValveWithOpenLimitSwitch(ResourceManager::ValveWithOpenLimitSwitchRole role)
{
    return getEnumKeyWithException(valvesWithOpenLimitSwitch,role,"valveWithOpenLimitSwitch role");
}
void ResourceManager::registerMideaController(std::shared_ptr<MideaWiredController> controller)
{
    if(mideaController.get()==nullptr){
        mideaController=controller;
    }
    else{
        throw runtime_error("Midea controler already registered");
    }
}

std::shared_ptr<MideaWiredController> ResourceManager::getMideaController()
{
    if(mideaController.get()==nullptr){
        throw runtime_error("No Midea controler registered");
    }
    else{
        return mideaController;
    }
}
void ResourceManager::setMainLoopPeriod(std::chrono::milliseconds newPeriod)
{
    mainLoopPeriod=newPeriod;
}
std::chrono::milliseconds ResourceManager::getMainLoopPeriod()
{
    return mainLoopPeriod;
}

void ResourceManager::registerAutoUpdatebleModbusDevice(std::shared_ptr<ModbusDevice> device)
{
    autoUpdatableModbusDevice.push_back(device);
}
void ResourceManager::updateModbusDeviceCaches()
{
    for_each(autoUpdatableModbusDevice.begin(),autoUpdatableModbusDevice.end(),[](auto it){it->updateCache();});
}








