#include "HCTRSettings.h"
#include <cJSON.h>
#include <sstream>
#include "parsers/rootParser.h"
#include "parsers/controllerParser.h"
#include "parsers/RootDeviceAbstractionParser.h"
#include "parsers/configurationParser.h"
#include "parsers/SettingsParser.h"
#include "parsers/rootDeviceParser.h"
#include "parsers/modbusControllerParser.h"
#include <fstream>
#include <string>
#include <iostream>

using namespace std;

void HCTRSettings::InitializeSystem(const char* settingFile)
{
    FILE* f = fopen(settingFile, "r");

    // Determine file size
    fseek(f, 0, SEEK_END);
    size_t size = ftell(f);

    char* fileContent = new char[size];
    if(!fileContent){
        throw runtime_error("not enough memory to parse settings file");
    }
    rewind(f);
    fread(fileContent, sizeof(char), size, f);
    fclose(f);
    cJSON *root=cJSON_Parse(fileContent);
    if(root){
        SettingsParser s;
        ModbusControllerParser mcp;
        ControllerParser c(mcp);
        RootDeviceParser rdp;
        ConfigurationParser cfg(rdp);
        RootDeviceAbstractionParser d;
        RootParser parser(cfg,c,s,d);
        parser.parse(root);
        delete[] fileContent;
    }
    else{
        delete[] fileContent;
        stringstream ss;
        ss<<"Unable to parse settings file: "<<settingFile;
        throw runtime_error(ss.str());
    }
}
