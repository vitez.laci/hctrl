#include "ParserManager.h"
#include "../../models/BAC_1000ALN.h"
#include "../../models/CWT_MB308K.h"
#include "../../framework/limitSwitch.h"
#include "../../framework/monostabilValve.h"
#include "../../framework/pump.h"
#include "../../framework/simpleOutput.h"
#include "BAC_1000ALNParser.h"
#include "CWT_MB308KParser.h"
#include "LimitSwitchParser.h"
#include "MonostabilValveParser.h"
#include "PumpParser.h"
#include "SimpleOutputParser.h"
#include "ValveWithOpenLimitSwitchParser.h"
#include "MideaWiredControllerParser.h"

#include "simpleDeviceParser.h"
#include <cstring>
#include <sstream>

std::function<ParserManager&()>ParserManager::instanceCreator=ParserManager::createOnlyInstance;
auto constCharStarComparator=[](const char* a, const char* b){return std::strcmp(a,b)<0;};
std::map<const char*,std::function<std::shared_ptr<IDeviceParser>()>,std::function<bool(const char*,const char*)>> ParserManager::deviceParserConstructors{
        {
            {BAC_1000ALN::typeName,[](){return std::make_shared<BAC_1000ALNParser>();}},
            {CWT_MB308K::typeName,[](){return std::make_shared<CWT_MB308KParser>();}},
            {MideaWiredControllerParser::typeName,[](){return std::make_shared<MideaWiredControllerParser>();}}
        },
        constCharStarComparator
    };
std::map<const char*,std::function<std::shared_ptr<MappedDeviceParser>()>,std::function<bool(const char*,const char*)>> ParserManager::mappedDeviceParserConstructors{
        {
            {LimitSwitch::typeName,[](){return std::make_shared<LimitSwitchParser>();}},
            {MonostabilValve::typeName,[](){return std::make_shared<MonostabilValveParser>();}},
            {Pump::typeName,[](){return std::make_shared<PumpParser>();}},
            {SimpleOutput::typeName,[](){return std::make_shared<SimpleOutputParser>();}}
        },
        constCharStarComparator
    };
std::map<const char*,std::function<std::shared_ptr<IDeviceAbstractionParser>()>,std::function<bool(const char*,const char*)>> ParserManager::deviceAbstractionParserConstructors{
    {
        {"Watts_22CX230NC4",[](){return std::make_shared<ValveWithOpenLimitSwitchParser>();}}
    },
    constCharStarComparator
};
std::shared_ptr<IDeviceParser> ParserManager::createDeviceParser(const char* name)
{
    if(!deviceParserConstructors.contains(name)){
        std::stringstream ss;
        ss<<"Unable to find device constructor: "<<name;
        throw std::runtime_error(ss.str());
    }
    return deviceParserConstructors.at(name)();
}
ParserManager & ParserManager::I()
{
    return instanceCreator();
}
ParserManager & ParserManager::createOnlyInstance()
{
    static ParserManager onlyInstance;
    return onlyInstance;
}
std::shared_ptr<MappedDeviceParser> ParserManager::createMappedDeviceParser(const char* name)
{
    if(!mappedDeviceParserConstructors.contains(name)){
        std::stringstream ss;
        ss<<"Unable to find mapped device constructor: "<<name;
        throw std::runtime_error(ss.str());
    }
    return mappedDeviceParserConstructors.at(name)();
}
std::shared_ptr<IDeviceAbstractionParser> ParserManager::createDeviceAbstractionParser(const char* name)
{
    if(!deviceAbstractionParserConstructors.contains(name)){
        std::stringstream ss;
        ss<<"Unable to find device abstraction parser constructor: "<<name;
        throw std::runtime_error(ss.str());
    }
    return deviceAbstractionParserConstructors.at(name)();
}



