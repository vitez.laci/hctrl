#include "MideaWiredControllerParser.h"
#include "../../resourceManager.h"
#include <memory>

using namespace std;
const char* MideaWiredControllerParser::typeName="MideaWiredController";
void MideaWiredControllerParser::parse(cJSON* root, std::shared_ptr<ModbusController> controller)
{
    SimpleDeviceParser::parse(root, controller);
    ResourceManager::I().registerMideaController(make_shared<MideaWiredController>(controller,name, address));
}
