#include "BAC_1000ALNParser.h"
#include "../../models/BAC_1000ALN.h"
#include "../../resourceManager.h"
#include <memory>

using namespace std;
void BAC_1000ALNParser::parse(cJSON* root, std::shared_ptr<ModbusController> controller)
{
    SimpleDeviceParser::parse(root,controller);
    auto instance=make_shared<BAC_1000ALN>(controller,name,address);
    ResourceManager::I().registerThermostat(name,instance);
    ResourceManager::I().registerAutoUpdatebleModbusDevice(instance);//write test for registration, generaliye behavior and refactor
    //investigate slowness of modbus comunication
}
