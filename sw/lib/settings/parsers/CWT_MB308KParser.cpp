#include "CWT_MB308KParser.h"
#include "../../models/CWT_MB308K.h"
#include "../../resourceManager.h"
#include <memory>

using namespace std;
void CWT_MB308KParser::registerIOhandler(std::shared_ptr<ModbusController> controller)
{
    ResourceManager::I().registerIOHandler(name,make_shared<CWT_MB308K>(controller,name,address));
}

MappingParser & CWT_MB308KParser::getMappingParser()
{
    return myParser;
}

