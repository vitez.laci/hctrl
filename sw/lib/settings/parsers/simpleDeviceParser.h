#ifndef SIMPLEDEVICEPARSER_H
#define SIMPLEDEVICEPARSER_H
#include <string>
#include "IDeviceParser.h"
class SimpleDeviceParser : public IDeviceParser
{
public:
    virtual ~SimpleDeviceParser()=default;
    void parse(cJSON * root, std::shared_ptr<ModbusController> ) override;
protected:
    const char* type;
    const char* name;
    uint16_t address;
};

#endif // SIMPLEDEVICEPARSER_H
