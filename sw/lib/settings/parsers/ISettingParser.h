#pragma once
#include <cJSON.h>
#include <cJSON_Utils.h>
class ISettingParser{
public:
    virtual ~ISettingParser()=default;
    virtual void parse(cJSON *root)=0;
};
