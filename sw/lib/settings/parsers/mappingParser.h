#ifndef MAPPINGPARSER_H
#define MAPPINGPARSER_H
#include <cJSON.h>
#include <memory>
#include "../../models/IIOHandler.h"

class MappingParser
{
public:
    virtual void parse(cJSON* root,std::shared_ptr<IIOHandler> ioHandler);
    virtual ~MappingParser()=default;
private:
    void doArrayParsing(cJSON*,std::shared_ptr<IIOHandler>);
};

#endif // MAPPINGPARSER_H
