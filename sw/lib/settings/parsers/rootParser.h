#ifndef ROOTPARSER_H
#define ROOTPARSER_H

#include "ISettingParser.h"

class RootParser :public ISettingParser
{
public:
    RootParser(ISettingParser& configurationParser,ISettingParser& controllerParser,ISettingParser& settingParser, ISettingParser& deviceAbstractionParser);
    void parse(cJSON* root) override;
private:
    void readValueAndDelegateParsing(cJSON* root, const char*,ISettingParser&, bool obligatory);
    ISettingParser& configurationParser;
    ISettingParser& controllerParser;
    ISettingParser& settingParser;
    ISettingParser& deviceAbstractionParser;
};

#endif // ROOTPARSER_H
