#ifndef CONFIGURATIONPARSER_H
#define CONFIGURATIONPARSER_H

#include "ISettingParser.h"
#include "rootDeviceParser.h"
class ConfigurationParser: public ISettingParser
{
public:
    void parse(cJSON * root) override;
    ConfigurationParser(RootDeviceParser&);
private:
    RootDeviceParser& deviceParser;
};

#endif // CONFIGURATIONPARSER_H
