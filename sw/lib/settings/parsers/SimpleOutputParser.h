#ifndef SIMPLEOUTPUTPARSER_H
#define SIMPLEOUTPUTPARSER_H
#include "SimpleMappedDeviceParser.h"
#include "../../framework/simpleOutput.h"
#include "../../resourceManager.h"
class SimpleOutputParser : public SimpleMappedDeviceParser<ResourceManager::SimpleOutputRole,SimpleOutput>
{
protected:
    std::shared_ptr<myDeviceType> createMappedDevice(std::shared_ptr<IIOHandler> ioHandler) override;
    void registerInstance(std::shared_ptr<myDeviceType> ) override;
};

#endif // SIMPLEOUTPUTPARSER_H
