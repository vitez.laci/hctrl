#include "modbusControllerParser.h"
#include "parserHelper.h"
#include <exception>
#include <sstream>
#include <cstring>
using namespace std;
std::shared_ptr<ModbusController> ModbusControllerParser::getInstance()
{
    return instance;
}
void ModbusControllerParser::parse(cJSON* root)
{
    const char* type=nullptr;
    const char* port=nullptr;
    
    const char *parity=nullptr;
    const char* comment=nullptr;
    ParserHelper helper(root);
    std::stringstream ss;
    ss<<"Invalid type for modbus controller parser: ["<<type<<"]";
    helper.tryStringParse("type",&type, ss.str().c_str());
        
    try{
        helper.tryStringParse("comment",&comment,"");
    }
    catch(runtime_error&){
        comment="";
    }
    
    helper.tryStringParse("parity",&parity,"Invalid or missing parity for modbus controller parser");
    if(strlen(parity)==0){
        throw std::runtime_error("Invalid or missing parity for modbus controller parser");
    }
    
    helper.tryStringParse("port",&port,"Invalid or missing port for modbus controller parser");
    double baudrate=helper.tryNumericParse("baudrate","Invalid or missing baudrate for modbus controller parser");
    double databit=helper.tryNumericParse("databit","Invalid or missing databit for modbus controller parser");
    double stopbit=helper.tryNumericParse("stopbit","Invalid or missing stopbit for modbus controller parser");
    
    instance=std::make_shared<ModbusController>(port,baudrate,parity[0],databit,stopbit,comment);
}

