#ifndef CONTROLLERPARSER_H
#define CONTROLLERPARSER_H

#include "ISettingParser.h"
#include "modbusControllerParser.h"
class ControllerParser :public ISettingParser
{
public:
    ControllerParser(ModbusControllerParser& modbusControllerParser);
    void parse(cJSON * root) override;
private:
    ModbusControllerParser& modbusControllerParser;
};

#endif // CONTROLLERPARSER_H
