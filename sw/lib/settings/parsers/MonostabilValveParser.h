#ifndef MONOSTABILVALVEPARSER_H
#define MONOSTABILVALVEPARSER_H

#include "SimpleMappedDeviceParser.h"
#include "../../resourceManager.h"
#include "../../framework/monostabilValve.h"
class MonostabilValveParser :public SimpleMappedDeviceParser<ResourceManager::MonostabilValveRole,MonostabilValve>
{
protected:
    void doAdditionalParameterParsing(cJSON * root) override;
    std::shared_ptr<myDeviceType> createMappedDevice(std::shared_ptr<IIOHandler> ioHandler) override;
    void registerInstance(std::shared_ptr<myDeviceType> ) override;
    MonostabilValve::DefaultState defaultState;
};

#endif // MONOSTABILVALVEPARSER_H
