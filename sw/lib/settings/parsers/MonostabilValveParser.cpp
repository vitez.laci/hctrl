#include "MonostabilValveParser.h"
#include "parserHelper.h"
#include "../../resourceManager.h"
using namespace std;

void MonostabilValveParser::doAdditionalParameterParsing(cJSON* root)
{
    defaultState=ParserHelper(root).tryParseEnum<decltype(defaultState)>("defaultState","Invalid default state for monostabil valve.");
}

std::shared_ptr<MonostabilValve> MonostabilValveParser::createMappedDevice(std::shared_ptr<IIOHandler> ioHandler)
{
    return make_shared<MonostabilValve>(ioHandler->getOutputPort(port),defaultState);
}
void MonostabilValveParser::registerInstance(std::shared_ptr<MonostabilValve> instance)
{
    ResourceManager::I().registerMonostabilValve(role,instance);
}


