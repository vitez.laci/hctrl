#ifndef SETTINGSPARSER_H
#define SETTINGSPARSER_H
#include "ISettingParser.h"
class SettingsParser : public ISettingParser
{
public:
    void parse(cJSON * root) override;
};

#endif // SETTINGSPARSER_H
