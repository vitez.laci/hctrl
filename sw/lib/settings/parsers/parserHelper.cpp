#include "parserHelper.h"
#include <stdexcept>
#include <sstream>

using namespace std;

ParserHelper::ParserHelper(cJSON* root)
:root(root)
{
}
double ParserHelper::tryNumericParse(const char* key, const char* exceptionMessage)
{
    cJSON* item=cJSON_GetObjectItem(root,key);
    if(item){
        if(cJSON_IsNumber(item)){
            return cJSON_GetNumberValue(item);
        }
    }
    stringstream ss;
    ss<<exceptionMessage<<". Key: "<<key;
    throw runtime_error(ss.str());
}
void ParserHelper::tryStringParse(const char* key, const char ** value, const char* exceptionMessage)
{
    cJSON* item=cJSON_GetObjectItem(root,key);
    stringstream ss;
    if(item){
        if(cJSON_IsString(item)){
            *value=cJSON_GetStringValue(item);
            return;
        }
        ss<<exceptionMessage<<". Item Not a string. Key: "<<key;
    }
    else{
        ss<<exceptionMessage<<". Item is missing. Key: "<<key;
    }
    throw runtime_error(ss.str());
}


