#include "IOHandlerParser.h"
#include "parserHelper.h"
#include "../../resourceManager.h"
#include <stdexcept>
#include <sstream>

using namespace std;

void IOHandlerParser::parse(cJSON* root, std::shared_ptr<ModbusController> controller)
{
    SimpleDeviceParser::parse(root,controller);
    registerIOhandler(controller);
    cJSON* mappingRoot=cJSON_GetObjectItem(root,"mapping");
    if(mappingRoot){
        getMappingParser().parse(mappingRoot,ResourceManager::I().getIOHandler(name));
    }
    else{
        stringstream ss;
        ss<<"Missing or invalid mapping for device "<<name;
        throw runtime_error(ss.str());
    }
    
}


