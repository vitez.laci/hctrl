#ifndef IOEXPANDERPARSER_H
#define IOEXPANDERPARSER_H

#include "simpleDeviceParser.h"
#include "mappingParser.h"
#include "../../models/IIOHandler.h"
#include <memory>

class IOHandlerParser : public SimpleDeviceParser
{
public:
    virtual ~IOHandlerParser()=default;
    void parse(cJSON * root, std::shared_ptr<ModbusController> ) override;
protected:
    virtual void registerIOhandler(std::shared_ptr<ModbusController>)=0;
    virtual MappingParser& getMappingParser()=0;
};

#endif // IOEXPANDERPARSER_H
