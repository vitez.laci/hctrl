#include "simpleDeviceParser.h"
#include "parserHelper.h"
#include <sstream>
#include <limits>

using namespace std;

void SimpleDeviceParser::parse(cJSON* root, std::shared_ptr<ModbusController> controller)
{
    ParserHelper helper(root);
    const char* parsedString;
    
    helper.tryStringParse("type",&parsedString,"Invalid or missing type from device description");
    type=parsedString;
    
    helper.tryStringParse("name",&parsedString,"Invalid or missing name from device description");
    name=parsedString;
    
    double parsedNumeric=helper.tryNumericParse("address","Invalid or missing address from device description");
    if(parsedNumeric<0 || parsedNumeric>=std::numeric_limits<decltype(address)>::max()){
        stringstream ss;
        ss<<"Invalid address of device description. Address should be between 0 and "<<std::numeric_limits<decltype(address)>::max()<<". Address is: "<<parsedNumeric;
        throw runtime_error(ss.str());
    }
    address=parsedNumeric;
}
