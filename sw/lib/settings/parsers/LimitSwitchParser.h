#ifndef LIMITSWITCHPARSER_H
#define LIMITSWITCHPARSER_H
#include "SimpleMappedDeviceParser.h"
#include <memory>
#include "../../framework/limitSwitch.h"
#include "../../resourceManager.h"

class LimitSwitchParser : public SimpleMappedDeviceParser<ResourceManager::LimitSwitchRole,LimitSwitch>
{
public:
    virtual ~LimitSwitchParser()=default;
protected:
    virtual void doAdditionalParameterParsing(cJSON * root) override;
    virtual void registerInstance(std::shared_ptr<myDeviceType>)override;
    virtual std::shared_ptr<myDeviceType> createMappedDevice(std::shared_ptr<IIOHandler> ioHandler)override;
    LimitSwitch::Behavior behavior;
};

#endif // LIMITSWITCHPARSER_H
