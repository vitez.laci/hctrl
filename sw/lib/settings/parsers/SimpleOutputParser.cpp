#include "SimpleOutputParser.h"
#include <memory>

using namespace std;

std::shared_ptr<SimpleOutput> SimpleOutputParser::createMappedDevice(std::shared_ptr<IIOHandler> ioHandler)
{
    return make_shared<SimpleOutput>(ioHandler->getOutputPort(port));
}
void SimpleOutputParser::registerInstance(std::shared_ptr<SimpleOutput> instance)
{
    ResourceManager::I().registerSimpleOutput(role, instance);
}

