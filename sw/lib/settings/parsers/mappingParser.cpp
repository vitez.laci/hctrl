#include "mappingParser.h"
#include <cJSON.h>
#include "ParserManager.h"
#include "parserHelper.h"

void MappingParser::parse(cJSON* root, std::shared_ptr<IIOHandler> ioHandler)
{
    doArrayParsing(cJSON_GetObjectItem(root,"input"),ioHandler);
    doArrayParsing(cJSON_GetObjectItem(root,"output"),ioHandler);
}
void MappingParser::doArrayParsing(cJSON* arr,std::shared_ptr<IIOHandler> ioHandler)
{
    if(cJSON_IsArray(arr)){
        cJSON* it;
        cJSON_ArrayForEach(it,arr){
            ParserHelper helper(it);
            const char* type;
            helper.tryStringParse("type",&type,"Missing type from mapping");
            ParserManager::I().createMappedDeviceParser(type)->parse(it,ioHandler);
        }
    }
}

