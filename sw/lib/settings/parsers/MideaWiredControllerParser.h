#ifndef MIDEAWIREDCONTROLLERPARSER_H
#define MIDEAWIREDCONTROLLERPARSER_H

#include "simpleDeviceParser.h"
class MideaWiredControllerParser: public SimpleDeviceParser
{
public:
    static const char* typeName;
    void parse(cJSON * root, std::shared_ptr<ModbusController> ) override;
};

#endif // MIDEAWIREDCONTROLLERPARSER_H
