#include "PumpParser.h"
#include <memory>

using namespace std;

std::shared_ptr<Pump> PumpParser::createMappedDevice(std::shared_ptr<IIOHandler> ioHandler){
    return make_shared<Pump>(ioHandler->getOutputPort(port));
}
void PumpParser::registerInstance(std::shared_ptr<Pump> instance)
{
    ResourceManager::I().registerPump(role,instance);
}

