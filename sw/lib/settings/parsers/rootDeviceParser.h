#pragma once
#include "IDeviceParser.h"

class RootDeviceParser: public IDeviceParser
{
public:
    virtual ~RootDeviceParser()=default;
    virtual void parse(cJSON * root, std::shared_ptr<ModbusController>) override;
};

