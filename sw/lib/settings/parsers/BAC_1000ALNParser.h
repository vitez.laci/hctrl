#ifndef BAC_1000ALNPARSER_H
#define BAC_1000ALNPARSER_H

#include "simpleDeviceParser.h"
class BAC_1000ALNParser:public SimpleDeviceParser
{
public:
    void parse(cJSON * root, std::shared_ptr<ModbusController> ) override;
};

#endif // BAC_1000ALNPARSER_H
