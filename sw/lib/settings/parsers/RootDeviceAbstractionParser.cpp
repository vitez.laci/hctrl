#include "RootDeviceAbstractionParser.h"
#include "parserHelper.h"
#include "ParserManager.h"

void RootDeviceAbstractionParser::parse(cJSON* root)
{
    if(cJSON_IsArray(root)){
        cJSON* iterator;
        cJSON_ArrayForEach(iterator,root){
            if(cJSON_IsObject(iterator)){
                ParserHelper helper(iterator);
                const char* type;
                helper.tryStringParse("type",&type,"invalid or missing type from deviceAbstraction item");
                ParserManager::I().createDeviceAbstractionParser(type)->parse(iterator);
            }
        }
    }
    else{
    throw std::runtime_error("Root is not an array of deviceAbstraction section");
    }
}
