#include "configurationParser.h"
#include "parserHelper.h"
#include "../../resourceManager.h"
#include <exception>
#include <sstream>

using namespace std;

ConfigurationParser::ConfigurationParser(RootDeviceParser& deviceParser)
:deviceParser(deviceParser)
{
}
void ConfigurationParser::parse(cJSON* root)
{
    if(cJSON_IsArray(root)){
        cJSON* iterator;
        cJSON_ArrayForEach(iterator,root){
            auto controllerItem=cJSON_GetObjectItem(iterator,"controller");
            if(!controllerItem){
                throw runtime_error("Missing controller specification from configuration");
            }
            auto controllerName=cJSON_GetStringValue(controllerItem);
            if(!controllerName){
                throw runtime_error("Controller nam is not a string");
            }
            auto controller=ResourceManager::I().getModbusController(controllerName);
            if(!controller){
                stringstream ss;
                ss<<"No contoller defined wit the following name: <"<<controllerName<<">";
                throw runtime_error(ss.str());
            }
            auto devices=cJSON_GetObjectItem(iterator,"devices");
            if(!devices){
                throw runtime_error("Missing devices block");
            }
            deviceParser.parse(devices,controller);
        }
    }
    else{
        throw runtime_error("Invalid configuration definition. It should be an array.");
    }
}
