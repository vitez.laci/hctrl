#include "rootDeviceParser.h"
#include "parserHelper.h"
#include "ParserManager.h"
#include "../../resourceManager.h"
#include <cstring>
#include <memory>
#include <exception>

using namespace std;

void RootDeviceParser::parse(cJSON* root, std::shared_ptr<ModbusController> modbusController)
{
    if(cJSON_IsArray(root)){
        cJSON* iterator;
        cJSON_ArrayForEach(iterator,root){
            ParserHelper helper(iterator);
            const char* type;
            helper.tryStringParse("type",&type,"missing type from device block");
            ParserManager::I().createDeviceParser(type)->parse(iterator,modbusController);
        }
    }
    else{
        throw runtime_error("Not an array root for devices");
    }
}


