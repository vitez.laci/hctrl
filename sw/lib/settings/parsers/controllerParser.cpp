#include "controllerParser.h"
#include <cstring>
#include "../../models/modbusController.h"
#include "../../resourceManager.h"
#include <sstream>
#include <memory>

ControllerParser::ControllerParser(ModbusControllerParser& modbusControllerParser)
:modbusControllerParser(modbusControllerParser)
{
}

void ControllerParser::parse(cJSON* root)
{
    if(cJSON_IsArray(root)){
        cJSON* iterator;
        cJSON_ArrayForEach(iterator,root){
            cJSON* item=iterator->child;
            if(item==NULL){
                throw std::runtime_error("Invalid atomic item in controller array.");
            }
            cJSON* type= cJSON_GetObjectItem(item,"type");
            if(type==NULL){
                std::stringstream ss;
                ss<<"Invalid item in controller array. Object doesn't have type field "<<item->string;
                throw std::runtime_error(ss.str());
            }
            if(strcmp(cJSON_GetStringValue(type),ModbusController::typeName)==0){
                modbusControllerParser.parse(item);
                ResourceManager::I().registerModbusController(item->string,modbusControllerParser.getInstance());
            }
        }
    }
    else{
        throw std::runtime_error("invalid block passed to controller parser. it is not an array");
    }
}
