#ifndef PUMPPARSER_H
#define PUMPPARSER_H

#include "SimpleMappedDeviceParser.h"
#include "../../resourceManager.h"
#include "../../framework/pump.h"

class PumpParser:public SimpleMappedDeviceParser<ResourceManager::PumpRole,Pump>
{
protected:
    std::shared_ptr<myDeviceType> createMappedDevice(std::shared_ptr<IIOHandler> ioHandler) override;
    void registerInstance(std::shared_ptr<myDeviceType> ) override;
};

#endif // PUMPPARSER_H
