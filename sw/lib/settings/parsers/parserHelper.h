#ifndef PARSERHELPER_H
#define PARSERHELPER_H
#include <cJSON.h>
#include <magic_enum.hpp>
#include <sstream>

class ParserHelper
{
public:
    ParserHelper(cJSON* root);
    double tryNumericParse(const char* key, const char* exceptionMessage);
    void tryStringParse(const char* key, const char** value, const char* exceptionMessage);
    template <typename Tenum>
    Tenum tryParseEnum(const char* key,const char* exceptionMessage){
        const char* enumText;
        std::stringstream ss;
        ss<<"invalid or missing enum text. Original exception message for trace: "<<exceptionMessage;
        tryStringParse(key,&enumText,ss.str().c_str());
        auto parsedEnum=magic_enum::enum_cast<Tenum>(enumText);
        if(!parsedEnum.has_value()){
            std::stringstream ss;
            ss<<exceptionMessage<<" Raw enum text is"<<enumText;
            throw std::runtime_error(exceptionMessage);
        }
        return parsedEnum.value();
    }
private:
    cJSON* root;
};

#endif // PARSERHELPER_H
