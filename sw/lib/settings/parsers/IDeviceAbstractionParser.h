#pragma once
#include <cJSON.h>
struct IDeviceAbstractionParser{
    virtual ~IDeviceAbstractionParser()=default;
    virtual void parse(cJSON* root)=0;
};
