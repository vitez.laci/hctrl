#ifndef VALVEWITHOPENLIMITSWITCHPARSER_H
#define VALVEWITHOPENLIMITSWITCHPARSER_H

#include "DeviceAbstractionParser.h"
#include "../../resourceManager.h"
#include "../../framework/valveWithOpenLimitSwitch.h"
#include <chrono>

class ValveWithOpenLimitSwitchParser : public DeviceAbstractionParser<ResourceManager::ValveWithOpenLimitSwitchRole,ValveWithOpenLimitSwitch>
{
public:
    virtual ~ValveWithOpenLimitSwitchParser()=default;
protected:
    ResourceManager::LimitSwitchRole inputRole;
    ResourceManager::MonostabilValveRole outputRole;
    std::chrono::milliseconds operationErrorThreshold;
    virtual void doAdditionalParameterParsing(cJSON * root) override;
    virtual std::shared_ptr<myDeviceType> createAbstractedDevice() override;
    virtual void registerInstance(std::shared_ptr<myDeviceType> ) override;
};

#endif // VALVEWITHOPENLIMITSWITCHPARSER_H
