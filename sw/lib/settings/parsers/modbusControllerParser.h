#ifndef MODBUSCONTROLLERPARSER_H
#define MODBUSCONTROLLERPARSER_H

#include "ISettingParser.h"
#include "../../models/modbusController.h"
#include <memory>

class ModbusControllerParser :ISettingParser
{
public:
    virtual ~ModbusControllerParser()=default;
    virtual void parse(cJSON * root) override;
    virtual std::shared_ptr<ModbusController> getInstance();
private:
    std::shared_ptr<ModbusController> instance;
};

#endif // MODBUSCONTROLLERPARSER_H
