#ifndef DEVICEABSTRACTIONPARSER_H
#define DEVICEABSTRACTIONPARSER_H
#include <cJSON.h>
#include <memory>
#include "IDeviceAbstractionParser.h"
#include "parserHelper.h"
#include "ParserManager.h"

template<typename TKey,typename TDevice>
class DeviceAbstractionParser: public IDeviceAbstractionParser
{
protected:
    typedef TDevice myDeviceType;
    TKey role;
    virtual void doAdditionalParameterParsing(cJSON * root){}
    virtual void registerInstance(std::shared_ptr<myDeviceType>)=0;
    virtual std::shared_ptr<myDeviceType> createAbstractedDevice()=0;
public:
    virtual ~DeviceAbstractionParser()=default;
    virtual void parse(cJSON* root) override{
        ParserHelper helper(root);
        const char* roleText;
        helper.tryStringParse("role",&roleText,"invalid or missing role from abstracted device");
        role=helper.tryParseEnum<TKey>("role","Unable to find role for abstracted device");
        doAdditionalParameterParsing(root);
        registerInstance(createAbstractedDevice());
    };

};

#endif // DEVICEABSTRACTIONPARSER_H
