#ifndef CWT_MB308KPARSER_H
#define CWT_MB308KPARSER_H
#include "IOHandlerParser.h"
#include "mappingParser.h"
class CWT_MB308KParser : public IOHandlerParser{
public:
    void registerIOhandler(std::shared_ptr<ModbusController>) override;
    MappingParser & getMappingParser() override;
private:
    MappingParser myParser;
};

#endif // CWT_MB308KPARSER_H
