#pragma once
#include "IDeviceParser.h"
#include "MappedDeviceParser.h"
#include "IDeviceAbstractionParser.h"
#include <map>
#include <memory>
#include <functional>

class ParserManager
{
public:
    virtual ~ParserManager()=default;
    virtual std::shared_ptr<MappedDeviceParser> createMappedDeviceParser(const char* name);
    virtual std::shared_ptr<IDeviceParser> createDeviceParser(const char* name);
    virtual std::shared_ptr<IDeviceAbstractionParser> createDeviceAbstractionParser(const char* name);
    static ParserManager& I();
protected:
    static std::function<ParserManager&()>instanceCreator;
    ParserManager()=default;
private:
    static std::map<const char*,std::function<std::shared_ptr<MappedDeviceParser>()>,std::function<bool(const char*,const char*)>> mappedDeviceParserConstructors;
    static std::map<const char*,std::function<std::shared_ptr<IDeviceParser>()>,std::function<bool(const char*,const char*)>> deviceParserConstructors;
    static std::map<const char*,std::function<std::shared_ptr<IDeviceAbstractionParser>()>,std::function<bool(const char*,const char*)>> deviceAbstractionParserConstructors;
    static ParserManager&createOnlyInstance();
};

