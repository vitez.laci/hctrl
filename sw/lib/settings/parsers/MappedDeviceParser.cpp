#include "MappedDeviceParser.h"
#include "parserHelper.h"
#include <limits>
#include <cstdint>
#include "../../resourceManager.h"
#include <sstream>

using namespace std;

void MappedDeviceParser::parse(cJSON* root, std::shared_ptr<IIOHandler> ioHandler)
{
    if(cJSON_IsObject(root)){
        ParserHelper helper(root);
        double num=helper.tryNumericParse("port","invalid or missing port from mapped device definition");
        helper.tryStringParse("displayName",&displayName,"invalid or missing display name from mapped device definition");
        helper.tryStringParse("type",&type,"invalid or missing type from mapped device definition");
        
        if(num<0 || num > numeric_limits<uint16_t>::max()){
            stringstream ss;
            ss<<"port is out of range for limit switch: "<<num;
            throw runtime_error(ss.str());
        }
        port=num;
    }
    else{
        throw runtime_error("mapped device parser is selected for parse not an object type");
    }
}

