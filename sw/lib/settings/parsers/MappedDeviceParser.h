#pragma once
#include <cJSON.h>
#include <memory>
#include "../../models/IIOHandler.h"



class MappedDeviceParser
{
public:
    virtual ~MappedDeviceParser()=default;
    virtual void parse(cJSON* root, std::shared_ptr<IIOHandler> ioHandler);
protected:
    uint16_t port;
    const char* type;
    const char* displayName;
};
