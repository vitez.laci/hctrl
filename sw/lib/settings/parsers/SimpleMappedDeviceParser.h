#pragma once
#include "MappedDeviceParser.h"
#include <functional>
#include <memory>
#include <magic_enum.hpp>
#include <sstream>
#include "parserHelper.h"


template<typename TKey,typename TDevice>
class SimpleMappedDeviceParser: public MappedDeviceParser{
protected:
    typedef TDevice myDeviceType;
    TKey role;
    virtual ~SimpleMappedDeviceParser()=default;
    virtual void doAdditionalParameterParsing(cJSON * root){}
    virtual void registerInstance(std::shared_ptr<myDeviceType>)=0;
    virtual std::shared_ptr<myDeviceType> createMappedDevice(std::shared_ptr<IIOHandler> ioHandler)=0;
public:
    void parse(cJSON * root,  std::shared_ptr<IIOHandler> ioHandler) final override{
        MappedDeviceParser::parse(root,ioHandler);
        ParserHelper helper(root);
        const char* roleText;
        helper.tryStringParse("role",&roleText,"invalid or missing role from mapped device");
        role=helper.tryParseEnum<TKey>("role","Unable to find role for mapped device");
        doAdditionalParameterParsing(root);
        registerInstance(createMappedDevice(ioHandler));
    }
};
