#include "ValveWithOpenLimitSwitchParser.h"
#include <memory>
#include "../../resourceManager.h"
using namespace std;

void ValveWithOpenLimitSwitchParser::doAdditionalParameterParsing(cJSON* root)
{
    ParserHelper helper(root);
    inputRole=helper.tryParseEnum<decltype(inputRole)>("inputRole","unable to find input role for ValveWithOpenLimitSwitch");
    outputRole=helper.tryParseEnum<decltype(outputRole)>("outputRole","unable to find output role for ValveWithOpenLimitSwitch");
    operationErrorThreshold= chrono::milliseconds(static_cast<uint64_t>(helper.tryNumericParse("timeLimitInMs","unable to time limit for ValveWithOpenLimitSwitch")));
    
}

std::shared_ptr<ValveWithOpenLimitSwitch> ValveWithOpenLimitSwitchParser::createAbstractedDevice()
{
    return make_shared<ValveWithOpenLimitSwitch>(
        ResourceManager::I().getMonostabilValve(outputRole),
        ResourceManager::I().getLimitSwitch(inputRole),
        make_shared<ValveErrorDetector>(operationErrorThreshold)
    );
}
void ValveWithOpenLimitSwitchParser::registerInstance(std::shared_ptr<ValveWithOpenLimitSwitch> instance)
{
    ResourceManager::I().registerValveWithOpenLimitSwitch(role,instance);
}

