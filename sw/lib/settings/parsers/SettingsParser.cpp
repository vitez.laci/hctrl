#include "SettingsParser.h"
#include "../../resourceManager.h"
#include <chrono>
#include <cstdint>
#include "parserHelper.h"

using namespace std;
void SettingsParser::parse(cJSON* root)
{
    ResourceManager::I().setMainLoopPeriod(chrono::milliseconds(
        static_cast<uint64_t>(ParserHelper(root).tryNumericParse("mainLoopPeriodInMs","Unable to parse main loop period"))
                                                   )
    );
}
