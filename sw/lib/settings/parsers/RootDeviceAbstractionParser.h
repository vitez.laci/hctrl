#ifndef ROOTDEVICEABSTRACTIONPARSER_H
#define ROOTDEVICEABSTRACTIONPARSER_H

#include "ISettingParser.h"
class RootDeviceAbstractionParser :public ISettingParser
{
public:
    void parse(cJSON * root) override;
};

#endif // ROOTDEVICEABSTRACTIONPARSER_H
