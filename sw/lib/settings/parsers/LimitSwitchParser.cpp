#include "LimitSwitchParser.h"
#include "parserHelper.h"
#include <limits>
#include <cstdint>
#include <magic_enum.hpp>
#include <sstream>

using namespace std;
void LimitSwitchParser::doAdditionalParameterParsing(cJSON* root)
{
    behavior=ParserHelper(root).tryParseEnum<LimitSwitch::Behavior>("behavior", "behavior is invalid for limit switch");
}
std::shared_ptr<LimitSwitch> LimitSwitchParser::createMappedDevice(std::shared_ptr<IIOHandler> ioHandler)
{
    return make_shared<LimitSwitch>(displayName,ioHandler->getInputPort(port),behavior);
}
void LimitSwitchParser::registerInstance(std::shared_ptr<LimitSwitch> instance)
{
    ResourceManager::I().registerLimitSwitch(role,instance);
}
