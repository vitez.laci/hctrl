#include "rootParser.h"
#include <sstream>
#include <exception>

RootParser::RootParser(ISettingParser& configurationParser,ISettingParser& controllerParser,ISettingParser& settingParser,ISettingParser& deviceAbstractionParser)
:configurationParser(configurationParser),controllerParser(controllerParser), settingParser(settingParser), deviceAbstractionParser(deviceAbstractionParser)
{
}


void RootParser::parse(cJSON* root)
{
    readValueAndDelegateParsing(root,"controllers",controllerParser, true);
    readValueAndDelegateParsing(root,"configurations",configurationParser, true);
    readValueAndDelegateParsing(root,"settings",settingParser, false);
    readValueAndDelegateParsing(root,"deviceAbstraction",deviceAbstractionParser, false);
}
void RootParser::readValueAndDelegateParsing(cJSON* root, const char* val, ISettingParser& parser, bool obligatory)
{
    cJSON* item=cJSON_GetObjectItem(root,val);
    if(item){
        parser.parse(item); 
    }
    else if(obligatory){
        std::stringstream ss;
        ss<<"Unable to find root item :" << val;
        throw std::runtime_error(ss.str());
    }
}


