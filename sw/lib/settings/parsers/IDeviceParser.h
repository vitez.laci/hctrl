#pragma once
#include <cJSON.h>
#include "../../models/modbusController.h"
#include <memory>

struct IDeviceParser{
    virtual ~IDeviceParser()=default;
    virtual void parse(cJSON * root, std::shared_ptr<ModbusController>)=0;
};
