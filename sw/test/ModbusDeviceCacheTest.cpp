#include <gtest/gtest.h>
#include "ModbusDeviceCache.h"

using namespace std;
using namespace testing;

TEST(ModbusDeviceCache,addressNormalization){
    {
    ModbusDeviceCache subject(40001,40004);
    EXPECT_EQ(0,subject.normalizedStartAddress);
    EXPECT_EQ(3,subject.normalizedEndAddress);
    }
    {
    ModbusDeviceCache subject(0,4);
    EXPECT_EQ(0,subject.normalizedStartAddress);
    EXPECT_EQ(4,subject.normalizedEndAddress);
    }
}
TEST(ModbusDeviceCache,addressSpace){
    ModbusDeviceCache subject(0,2);
    *subject.data()=3;
    *(subject.data()+1)=2;
    *(subject.data()+2)=1;
    EXPECT_EQ(subject[0],3);
    EXPECT_EQ(subject[1],2);
    EXPECT_EQ(subject[2],1);
}
TEST(ModbusDeviceCache,size){
    class TC: public ModbusDeviceCache{
    public:
        TC():ModbusDeviceCache(0,2){}
        size_t getCacheSize() override{return ModbusDeviceCache::getCacheSize();}
    };
    TC subject;
    EXPECT_EQ(subject.getCacheSize(),3);
}
