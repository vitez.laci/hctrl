#include <gtest/gtest.h>
#include <memory>
#include "settings/parsers/CWT_MB308KParser.h"
#include "MockResourceManager.h"
#include "MockMappingParser.h"
#include <models/CWT_MB308K.h>
#include "MockModbusController.h"


using namespace std;
using namespace testing;

TEST(CWT_MB308KParser, construction){
    std::shared_ptr<ModbusController> ctrl1(new MockModbusController());
    MockResourceManager resourceManager;
    MockMappingParser mappingParser;
    cJSON* root=cJSON_CreateObject();
    CWT_MB308KParser subject;
    cJSON_AddStringToObject(root,"type","tp");
    cJSON_AddStringToObject(root,"name","nm");
    cJSON_AddNumberToObject(root,"address",584);
    cJSON_AddObjectToObject(root,"mapping");
    
    EXPECT_CALL(resourceManager,registerIOHandler(StrEq("nm"),Pointer(WhenDynamicCastTo<const CWT_MB308K*>(_))))   ;
    subject.parse(root,ctrl1);
}
