#include <gmock/gmock.h>
#include "framework/I2WayValve.h"

class MockI2WayValve:public I2WayValve{
public:
    MOCK_METHOD(void,open,(),(override));
    MOCK_METHOD(void,close,(),(override));
};
