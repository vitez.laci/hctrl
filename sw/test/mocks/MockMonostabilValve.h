#pragma once
#include <gmock/gmock.h>
#include "framework/monostabilValve.h"
#include "MockOutputPort.h"
class MockMonostabilValve:public MonostabilValve
{
private:
    MockOutputPort<bool> outputPort;
public:
    MockMonostabilValve():MonostabilValve(outputPort,MonostabilValve::DefaultState::closed){}
};
