#include <gmock/gmock.h>
#include <models/IThermostat.h>
class MockIThermostat:public IThermostat{
public:
    MOCK_METHOD(State,getState,(),(override));
    MOCK_METHOD(void,setState,(),(override));
    MOCK_METHOD(FanSpeedSetting,getFanSpeedSetting,(),(override));
    MOCK_METHOD(void,setFanSpeedSetting,(FanSpeedSetting),(override));
    MOCK_METHOD(OperatonMode,getOperationMode,(),(override));
    MOCK_METHOD(void,setOperationMode,(OperatonMode),(override));
    MOCK_METHOD(float,getTemperaturePoint,(),(override));
    MOCK_METHOD(void,setTemperaturePoint,(float),(override));
    MOCK_METHOD(LockState,getLockState,(),(override));
    MOCK_METHOD(void,setLockState,(LockState),(override));
    MOCK_METHOD(float,getRoomTemerature,(),(override));
    MOCK_METHOD(bool,isCoolingValveOn,(),(override));
    MOCK_METHOD(bool,isHeatingValveOn,(),(override));
    MOCK_METHOD(OperatingFanSpeed,getFanSpeed,(),(override));
        
}; 
