#pragma once
#include <gmock/gmock.h>
#include "settings/parsers/ParserManager.h"
class MockParserManager:public ParserManager{
public:
    MockParserManager();
    MOCK_METHOD(std::shared_ptr<IDeviceParser>,createDeviceParser,(const char * name),(override));
    MOCK_METHOD(std::shared_ptr<MappedDeviceParser>,createMappedDeviceParser,(const char * name),(override));
    MOCK_METHOD(std::shared_ptr<IDeviceAbstractionParser>, createDeviceAbstractionParser,(const char* name),(override));
    MockParserManager& returnInstance();
};
