#include "MockParserManager.h"
#include <functional>

using namespace std;

MockParserManager::MockParserManager()
{
    instanceCreator=bind(&MockParserManager::returnInstance,this);
}

MockParserManager & MockParserManager::returnInstance()
{
    return *this;
}

