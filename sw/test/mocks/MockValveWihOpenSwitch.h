#pragma once
#include "framework/valveWithOpenLimitSwitch.h"
#include "MockI2wayValve.h"
#include "MockLimitSwitch.h"
#include "MockValveErrorDetector.h"
#include <memory>
class MockValveWithOpenLimitSwitch: public ValveWithOpenLimitSwitch{
public:
    MockValveWithOpenLimitSwitch():
    ValveWithOpenLimitSwitch(
        std::make_shared<MockI2WayValve>(),
        std::make_shared<MockLimitSwith>(),
        std::make_shared<MockValveErrorDetector>()
    ){}
};
