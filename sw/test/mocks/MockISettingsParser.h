#pragma once
#include <gmock/gmock.h>
#include <settings/parsers/ISettingParser.h>

class MockISettingsParser :public ISettingParser{
public:
    MOCK_METHOD(void,parse,(cJSON*),(override));
};
