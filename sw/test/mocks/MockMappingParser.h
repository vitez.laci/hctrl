#pragma once
#include <gmock/gmock.h>
#include "settings/parsers/mappingParser.h"

class MockMappingParser:public MappingParser{
public:
    MOCK_METHOD(void,parse,(cJSON* root,std::shared_ptr<IIOHandler> ioHandler),(override));
};
