#pragma once
#include <gmock/gmock.h>
#include "settings/parsers/DeviceAbstractionParser.h"

enum class k{A,B};
class TestClass{};
class MockDeviceAbstractionParser: public DeviceAbstractionParser<k,TestClass>{
public:
    MOCK_METHOD(void ,registerInstance,(std::shared_ptr<myDeviceType>),(override));
    MOCK_METHOD(std::shared_ptr<myDeviceType> ,createAbstractedDevice,(),(override));
    MOCK_METHOD(void ,parse,(cJSON* root) ,(override));
    MOCK_METHOD(void ,doAdditionalParameterParsing,(cJSON* root) ,(override));
    
};
