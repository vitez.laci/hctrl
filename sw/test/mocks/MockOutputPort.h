#pragma once
#include "framework/IOutputPort.h"
#include <gmock/gmock.h>

template <typename T>
class MockOutputPort:public IOutputPort<T>{
public:
    MOCK_METHOD(void,write,(T),(override));
};

