#pragma once
#include <gmock/gmock.h>
#include "MockInputPort.h"
#include "framework/limitSwitch.h"

class MockLimitSwith:public LimitSwitch{
private:
    MockInputPort<bool> input;
public:
    MockLimitSwith():LimitSwitch("name",input,LimitSwitch::Behavior::trueOnEndpoint){}
    MOCK_METHOD(LimitSwitch::State,getState,(),(override));
    MOCK_METHOD(std::string&,getName,(),(override));
};
