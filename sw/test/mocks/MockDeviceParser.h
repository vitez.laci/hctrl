#pragma once
#include <gmock/gmock.h>
#include "settings/parsers/IDeviceParser.h"

class MockDeviceParser: public IDeviceParser{
public:
    MOCK_METHOD(void,parse,(cJSON*, std::shared_ptr<ModbusController>),(override));
};
