#pragma once
#include <gmock/gmock.h>
#include "settings/parsers/MappedDeviceParser.h"

class MockMappedDeviceParser:public MappedDeviceParser {
public:
    MOCK_METHOD(void, parse,(cJSON * root, std::shared_ptr<IIOHandler> ioHandler) ,(override));
};
