#pragma once
#include <gmock/gmock.h>
#include "settings/parsers/modbusControllerParser.h"
class MockModbusParser: public ModbusControllerParser{
public:
    MOCK_METHOD(void,parse,(cJSON * root),(override));
    MOCK_METHOD(std::shared_ptr<ModbusController>,getInstance,(),(override));
};
