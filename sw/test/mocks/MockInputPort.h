#pragma once
#include "framework/IInputPort.h"
#include <gmock/gmock.h>

template <typename T>
class MockInputPort:public IInputPort<T>{
public:
    MOCK_METHOD(T,read,(),(override));
};
