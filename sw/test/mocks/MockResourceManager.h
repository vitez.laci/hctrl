#pragma once
#include <gmock/gmock.h>
#include <resourceManager.h>

class MockResourceManager :public ResourceManager{
public:
    MockResourceManager();
    MOCK_METHOD(void,registerModbusController,(const char*, std::shared_ptr<ModbusController>),(override));
    MOCK_METHOD(std::shared_ptr<ModbusController>, getModbusController,(const char*),(override));
    MOCK_METHOD(void,registerThermostat,(const char*, std::shared_ptr<IThermostat>),(override));
    MOCK_METHOD(std::shared_ptr<IThermostat>,getThermostat,(const char*),(override));
    MOCK_METHOD(void,registerIOHandler,(const char*, std::shared_ptr<IIOHandler>),(override));
    MOCK_METHOD(std::shared_ptr<IIOHandler>,getIOHandler,(const char*),(override));
    MOCK_METHOD(void, registerLimitSwitch,(LimitSwitchRole, std::shared_ptr<LimitSwitch>),(override));
    MOCK_METHOD(std::shared_ptr<LimitSwitch>, getLimitSwitch,(LimitSwitchRole),(override));
    MOCK_METHOD(void, registerMonostabilValve,(MonostabilValveRole, std::shared_ptr<MonostabilValve>),(override));
    MOCK_METHOD(std::shared_ptr<MonostabilValve>, getMonostabilValve,(MonostabilValveRole),(override));
    MOCK_METHOD(void, registerPump,(PumpRole, std::shared_ptr<Pump>),(override));
    MOCK_METHOD(std::shared_ptr<Pump>, getPump,(PumpRole),(override));
    MOCK_METHOD(void, registerSimpleOutput,(SimpleOutputRole, std::shared_ptr<SimpleOutput>),(override));
    MOCK_METHOD(std::shared_ptr<SimpleOutput>, getSimpleOutput,(SimpleOutputRole),(override));
    MOCK_METHOD(void, registerValveWithOpenLimitSwitch,(ValveWithOpenLimitSwitchRole, std::shared_ptr<ValveWithOpenLimitSwitch>),(override));
    MOCK_METHOD(std::shared_ptr<ValveWithOpenLimitSwitch>, getValveWithOpenLimitSwitch,(ValveWithOpenLimitSwitchRole),(override));
    MOCK_METHOD(void ,setMainLoopPeriod,(std::chrono::milliseconds),(override));
    MOCK_METHOD(std::chrono::milliseconds ,getMainLoopPeriod,(),(override));
    ResourceManager& createMockedResourceManager();
};
