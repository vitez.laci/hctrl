#include "MockResourceManager.h"
#include <functional>

using namespace std;
MockResourceManager::MockResourceManager()
{
    instanceCreator=bind(&MockResourceManager::createMockedResourceManager,this);
}

ResourceManager & MockResourceManager::createMockedResourceManager()
{
    return *this;
}

