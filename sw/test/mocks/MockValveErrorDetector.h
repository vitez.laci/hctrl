#pragma once
#include <gmock/gmock.h>
#include "framework/valveErrorDetector.h"
#include <chrono>
using std::operator""ms;
class MockValveErrorDetector:public ValveErrorDetector{
public:
    MockValveErrorDetector():ValveErrorDetector(100ms){}
    MOCK_METHOD(void,triggerAction,(),(override));
    MOCK_METHOD(void,approveAction,(),(override));
    MOCK_METHOD(bool,hasError,(),(override));
};
