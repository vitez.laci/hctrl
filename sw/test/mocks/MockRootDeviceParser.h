#pragma once
#include <gmock/gmock.h>
#include "./settings/parsers/rootDeviceParser.h"

class MockRootDeviceParser: public RootDeviceParser{
public:
    MOCK_METHOD(void,parse,(cJSON * root, std::shared_ptr<ModbusController>),(override));
};
