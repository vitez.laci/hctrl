#pragma once

#include <gmock/gmock.h>
#include "models/modbusDevice.h"
#include "MockModbusController.h"
#include <memory>

class MockModbusDevice :public ModbusDevice{
public:
    MockModbusDevice():ModbusDevice(std::make_shared<MockModbusController>(),"mock",0,{}){}
    MOCK_METHOD(void ,updateCache,(),(override));
};

