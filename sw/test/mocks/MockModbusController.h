#pragma once

#include <gmock/gmock.h>
#include "models/modbusController.h"

class MockModbusController: public ModbusController{
public:
    MockModbusController()=default;
    MOCK_METHOD(void ,readRegisters,(uint16_t deviceAddress,uint16_t startAddress, uint16_t endAddress, uint16_t* target),(override));
};
