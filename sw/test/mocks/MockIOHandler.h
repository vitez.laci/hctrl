#pragma once
#include <gmock/gmock.h>
#include "models/IIOHandler.h"
#include <cstdint>

class MockIOHandler:public IIOHandler {
public:
    MOCK_METHOD( IInputPort<bool>&, getInputPort,(uint16_t inputNoFrom0),(override));
    MOCK_METHOD( IOutputPort<bool>&, getOutputPort,(uint16_t outputNoFrom0),(override));
};
