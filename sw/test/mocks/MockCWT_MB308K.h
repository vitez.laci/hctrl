#pragma once
#include <gmock/gmock.h>
#include "models/CWT_MB308K.h"

class MockCWT_MB308K: public CWT_MB308K{
public:
    MockCWT_MB308K();
    MOCK_METHOD( IInputPort<bool>&, getInputPort,(uint16_t inputNoFrom0),(override));
    MOCK_METHOD( IOutputPort<bool>&, getOutputPort,(uint16_t outputNoFrom0),(override));
    MOCK_METHOD(uint16_t, readInput,(uint16_t portNo), (override));
    MOCK_METHOD( void ,writeOutput,(uint16_t portNo, uint16_t value),( override));
};
