#include <gtest/gtest.h>
#include "models/modbusDevice.h"
#include "MockModbusController.h"
#include "ModbusDeviceCache.h"
#include <memory>

using namespace std;
using namespace testing;

TEST(ModbusDevice, validCache){
    ModbusDevice subject(make_shared<MockModbusController>(),"m",5,{
        {"A",make_shared<ModbusDeviceCache>(0,0)},
        {"B",make_shared<ModbusDeviceCache>(10,15)}});
    subject.getCache("A");
    subject.getCache("B");
    try{
        subject.getCache("C");
        FAIL();
    }
    catch(runtime_error&){}
}
TEST(ModbusDevice, update){
    auto controller=make_shared<MockModbusController>();
    ModbusDevice subject(controller,"m",5,{
        {"A",make_shared<ModbusDeviceCache>(0,0)},
        {"B",make_shared<ModbusDeviceCache>(10,15)}});
    EXPECT_CALL(*controller,readRegisters(5,0,0,Pointer(subject.getCache("A").data()))).Times(1);
    EXPECT_CALL(*controller,readRegisters(5,10,15,Pointer(subject.getCache("B").data()))).Times(1);
    subject.updateCache();
}
