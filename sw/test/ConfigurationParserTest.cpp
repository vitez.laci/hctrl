#include <gtest/gtest.h>
#include "settings/parsers/configurationParser.h"
#include "MockRootDeviceParser.h"
#include "MockModbusController.h"
#include "MockResourceManager.h"
#include "resourceManager.h"
#include <iostream>
#include <memory>

using namespace std;
using namespace testing;

TEST(ConfigurationParser, valid){
    shared_ptr<ModbusController> controller1(new MockModbusController());
    shared_ptr<ModbusController> controller2(new MockModbusController());
    MockRootDeviceParser deviceParser;
    ConfigurationParser subject(deviceParser);
    MockResourceManager resourceManager;

    
    cJSON* root=cJSON_CreateArray();
    cJSON* item1=cJSON_CreateObject();
    cJSON_AddStringToObject(item1,"controller","A");
    cJSON *dev=cJSON_AddArrayToObject(item1,"devices");
    cJSON* item2=cJSON_CreateObject();
    cJSON_AddStringToObject(item2,"controller","B");
    cJSON *dev2=cJSON_AddArrayToObject(item2,"devices");
    cJSON_AddItemToArray(root,item1);
    cJSON_AddItemToArray(root,item2);
    
    EXPECT_CALL(resourceManager,getModbusController(StrEq("A"))).Times(1).WillOnce(Return(controller1));
    EXPECT_CALL(resourceManager,getModbusController(StrEq("B"))).Times(1).WillOnce(Return(controller2));
    EXPECT_CALL(deviceParser,parse(dev,Pointer(controller1.get()))).Times(1);
    EXPECT_CALL(deviceParser,parse(dev2,Pointer(controller2.get()))).Times(1);
    
    subject.parse(root);
    cJSON_Delete(root);
}
TEST(ConfigurationParser, rootIsnull){
    MockRootDeviceParser deviceParser;
    ConfigurationParser subject(deviceParser);
    try{
        subject.parse(nullptr);
        FAIL();
    }
    catch(std::runtime_error&){}
}
TEST(ConfigurationParser, missingDevice){
    shared_ptr<ModbusController> controller1(new MockModbusController());
    MockRootDeviceParser deviceParser;
    ConfigurationParser subject(deviceParser);
    MockResourceManager resourceManager;

    
    cJSON* root=cJSON_CreateArray();
    cJSON* item1=cJSON_CreateObject();
    cJSON_AddStringToObject(item1,"controller","A");
    cJSON_AddItemToArray(root,item1);
    
    EXPECT_CALL(resourceManager,getModbusController(StrEq("A"))).Times(1).WillOnce(Return(controller1));
    try{
        subject.parse(root);
        FAIL();
    }
    catch(std::runtime_error&){}
    cJSON_Delete(root);
}
TEST(ConfigurationParser, invalidControllerNameType){
    MockRootDeviceParser deviceParser;
    ConfigurationParser subject(deviceParser);
    MockResourceManager resourceManager;

    
    cJSON* root=cJSON_CreateArray();
    cJSON* item1=cJSON_CreateObject();
    cJSON_AddNumberToObject(item1,"controller",5);
    cJSON_AddArrayToObject(item1,"devices");
    cJSON_AddItemToArray(root,item1);
    
    try{
        subject.parse(root);
        FAIL();
    }
    catch(std::runtime_error&){}
    cJSON_Delete(root);
}
TEST(ConfigurationParser, noController){
    MockRootDeviceParser deviceParser;
    ConfigurationParser subject(deviceParser);
    MockResourceManager resourceManager;

    
    cJSON* root=cJSON_CreateArray();
    cJSON* item1=cJSON_CreateObject();
    cJSON_AddStringToObject(item1,"controller","A");
    cJSON_AddArrayToObject(item1,"devices");
    cJSON_AddItemToArray(root,item1);
    EXPECT_CALL(resourceManager,getModbusController(StrEq("A"))).Times(1).WillOnce(Return(nullptr));
    
    try{
        subject.parse(root);
        FAIL();
    }
    catch(std::runtime_error&){}
    cJSON_Delete(root);
}
TEST(ConfigurationParser, notAnArrayInput){
    MockRootDeviceParser deviceParser;
    ConfigurationParser subject(deviceParser);
    MockResourceManager resourceManager;

    
    
    cJSON* root=cJSON_CreateObject();
    
    try{
        subject.parse(root);
        FAIL();
    }
    catch(std::runtime_error&){}
    cJSON_Delete(root);
}
TEST(ConfigurationParser, controllerIsMissing){
    MockRootDeviceParser deviceParser;
    ConfigurationParser subject(deviceParser);
    MockResourceManager resourceManager;

    
    cJSON* root=cJSON_CreateArray();
    cJSON* item1=cJSON_CreateObject();
    cJSON_AddArrayToObject(item1,"devices");
    cJSON_AddItemToArray(root,item1);
    
    try{
        subject.parse(root);
        FAIL();
    }
    catch(std::runtime_error&){}
    cJSON_Delete(root);
}
