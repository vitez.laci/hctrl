#include <gtest/gtest.h>
#include "MockInputPort.h"
#include "framework/limitSwitch.h"

using testing::Return;

TEST(LimitSwitch,getState_trueOnEndpoint){
    MockInputPort<bool> input;
    LimitSwitch subject("name",input, LimitSwitch::Behavior::trueOnEndpoint);
    EXPECT_CALL(input,read())
                .Times(2)
                .WillOnce(Return(true))
                .WillOnce(Return(false))
                ;
    EXPECT_STREQ("name",subject.getName().c_str());
    EXPECT_EQ(LimitSwitch::State::onEndpoint,subject.getState());
    EXPECT_EQ(LimitSwitch::State::notOnEndpoint,subject.getState());
}
TEST(LimitSwitch,getState_falseOnEndpoint){
    MockInputPort<bool> input;
    LimitSwitch subject("",input, LimitSwitch::Behavior::falseOnEndpoint);
    EXPECT_CALL(input,read())
                .Times(2)
                .WillOnce(Return(true))
                .WillOnce(Return(false))
                ;
    EXPECT_EQ(LimitSwitch::State::notOnEndpoint,subject.getState());
    EXPECT_EQ(LimitSwitch::State::onEndpoint,subject.getState());
}
