#include <gtest/gtest.h>
#include <cJSON.h>
#include "models/modbusController.h"
#include "settings/parsers/modbusControllerParser.h"
#include <exception>

TEST(ModbusControllerParser,valid){
    ModbusControllerParser subject;
    cJSON* root=cJSON_CreateObject();
    cJSON_AddStringToObject(root,"type","ModbusController");
    cJSON_AddStringToObject(root,"port","prt");
    cJSON_AddNumberToObject(root,"baudrate",9600);
    cJSON_AddStringToObject(root,"parity","N");
    cJSON_AddStringToObject(root,"comment","comm");
    cJSON_AddNumberToObject(root,"databit",8);
    cJSON_AddNumberToObject(root,"stopbit",1.5);
    
    subject.parse(root);
    
    auto testInstance=subject.getInstance();
    EXPECT_EQ(testInstance->port,"prt");
    EXPECT_EQ(testInstance->baudrate,9600);
    EXPECT_EQ(testInstance->parity,'N');
    EXPECT_EQ(testInstance->comment,"comm");
    EXPECT_EQ(testInstance->databit,8);
    EXPECT_LE(testInstance->stopbit-1.5,5E-6);
    cJSON_Delete(root);
}
TEST(ModbusControllerParser,invalidType){
    ModbusControllerParser subject;
    cJSON* root=cJSON_CreateObject();
    cJSON_AddStringToObject(root,"type","invalidType");

    try{
    subject.parse(root);
    FAIL();
    }
    catch(std::runtime_error&){}
    
    cJSON_Delete(root);
}
TEST(ModbusControllerParser,commentMissing){
    ModbusControllerParser subject;
    cJSON* root=cJSON_CreateObject();
    cJSON_AddStringToObject(root,"type","ModbusController");
    cJSON_AddStringToObject(root,"port","prt");
    cJSON_AddNumberToObject(root,"baudrate",9600);
    cJSON_AddStringToObject(root,"parity","N");
    cJSON_AddNumberToObject(root,"databit",8);
    cJSON_AddNumberToObject(root,"stopbit",1.5);
    
    subject.parse(root);
    
    auto testInstance=subject.getInstance();
    EXPECT_EQ(testInstance->port,"prt");
    EXPECT_EQ(testInstance->baudrate,9600);
    EXPECT_EQ(testInstance->parity,'N');
    EXPECT_EQ(testInstance->comment,"");
    EXPECT_EQ(testInstance->databit,8);
    EXPECT_LE(testInstance->stopbit-1.5,5E-6);
    cJSON_Delete(root);
}

TEST(ModbusControllerParser,missingPort){
    ModbusControllerParser subject;
    cJSON* root=cJSON_CreateObject();
    cJSON_AddStringToObject(root,"type","ModbusController");
    cJSON_AddNumberToObject(root,"baudrate",9600);
    cJSON_AddStringToObject(root,"parity","N");
    cJSON_AddNumberToObject(root,"databit",8);
    cJSON_AddNumberToObject(root,"stopbit",1.5);
    
    try{
        subject.parse(root);
    FAIL();
    }
    catch(std::runtime_error&){}
    cJSON_Delete(root);
}
TEST(ModbusControllerParser,missingBaudrate){
    ModbusControllerParser subject;
    cJSON* root=cJSON_CreateObject();
    cJSON_AddStringToObject(root,"type","ModbusController");
    cJSON_AddStringToObject(root,"parity","N");
    cJSON_AddNumberToObject(root,"databit",8);
    cJSON_AddNumberToObject(root,"stopbit",1.5);
    cJSON_AddStringToObject(root,"port","prt");
    
    try{
        subject.parse(root);
    FAIL();
    }
    catch(std::runtime_error&){}
    cJSON_Delete(root);
}
TEST(ModbusControllerParser,missingParity){
    ModbusControllerParser subject;
    cJSON* root=cJSON_CreateObject();
    cJSON_AddStringToObject(root,"type","ModbusController");
    cJSON_AddNumberToObject(root,"databit",8);
    cJSON_AddNumberToObject(root,"stopbit",1.5);
    cJSON_AddStringToObject(root,"port","prt");
    cJSON_AddNumberToObject(root,"baudrate",9600);
    
    try{
        subject.parse(root);
    FAIL();
    }
    catch(std::runtime_error&){}
    cJSON_Delete(root);
}
TEST(ModbusControllerParser,missingDatabit){
    ModbusControllerParser subject;
    cJSON* root=cJSON_CreateObject();
    cJSON_AddStringToObject(root,"type","ModbusController");
    cJSON_AddNumberToObject(root,"stopbit",1.5);
    cJSON_AddStringToObject(root,"port","prt");
    cJSON_AddNumberToObject(root,"baudrate",9600);
    cJSON_AddStringToObject(root,"parity","N");
    
    try{
        subject.parse(root);
    FAIL();
    }
    catch(std::runtime_error&){}
    cJSON_Delete(root);
}
TEST(ModbusControllerParser,missingStopbit){
    ModbusControllerParser subject;
    cJSON* root=cJSON_CreateObject();
    cJSON_AddStringToObject(root,"type","ModbusController");
    cJSON_AddStringToObject(root,"port","prt");
    cJSON_AddNumberToObject(root,"baudrate",9600);
    cJSON_AddStringToObject(root,"parity","N");
    cJSON_AddNumberToObject(root,"databit",8);
    
    try{
        subject.parse(root);
    FAIL();
    }
    catch(std::runtime_error&){}
    cJSON_Delete(root);
}
