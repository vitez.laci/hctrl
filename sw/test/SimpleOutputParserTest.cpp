#include <gtest/gtest.h>

#include "settings/parsers/SimpleOutputParser.h"
#include "MockResourceManager.h"
#include "MockIOHandler.h"
#include "MockOutputPort.h"
#include <cJSON.h>
#include <memory>


using namespace std;
using namespace testing;

TEST(SimpleOutput, valid){
    
    shared_ptr<MockIOHandler> ioHandler(new MockIOHandler());
    SimpleOutputParser subject;
    MockResourceManager resourceManager;
    MockOutputPort<bool> outputPort;
    cJSON* root=cJSON_CreateObject();
    cJSON_AddNumberToObject(root,"port",5);
    cJSON_AddStringToObject(root,"type","LimitSwitch");
    cJSON_AddStringToObject(root,"role","SimulatedThermostatOutput");
    cJSON_AddStringToObject(root,"displayName","something");
    EXPECT_CALL(*ioHandler,getOutputPort(5)).Times(1).WillOnce(ReturnRef(outputPort));
    EXPECT_CALL(resourceManager,registerSimpleOutput(ResourceManager::SimpleOutputRole::SimulatedThermostatOutput,_));
    
    subject.parse(root,ioHandler);
}


