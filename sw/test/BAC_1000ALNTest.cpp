#include <gtest/gtest.h>
#include <memory>
#include "settings/parsers/BAC_1000ALNParser.h"
#include "MockResourceManager.h"
#include "models/BAC_1000ALN.h"
#include "MockModbusController.h"


using namespace std;
using namespace testing;

TEST(BAC_1000ALNParser,valid){
    std::shared_ptr<ModbusController> ctrl1(new MockModbusController());
    MockResourceManager resourceManager;
    cJSON* root=cJSON_CreateObject();
    BAC_1000ALNParser subject;
    cJSON_AddStringToObject(root,"type","tp");
    cJSON_AddStringToObject(root,"name","nm");
    cJSON_AddNumberToObject(root,"address",584);
    
    EXPECT_CALL(resourceManager,registerThermostat(StrEq("nm"),Pointer(WhenDynamicCastTo<const BAC_1000ALN*>(_))))   ;
    subject.parse(root,ctrl1);
    
}
