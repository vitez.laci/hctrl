#include <gtest/gtest.h>
#include <cJSON.h>
#include "settings/parsers/rootParser.h"
#include "mocks/MockISettingsParser.h"
#include <exception>
using namespace testing;
TEST(rootParser,controllers){
    cJSON* root=cJSON_CreateObject();
    cJSON* arr=cJSON_AddArrayToObject(root,"controllers");
    MockISettingsParser confParser,ctrlParser, settingParser, deviceAbstractionParser;
    RootParser subject(confParser,ctrlParser,settingParser,deviceAbstractionParser);
    EXPECT_CALL(ctrlParser,parse(arr)).Times(1);
    EXPECT_CALL(confParser,parse(_)).Times(0);
    EXPECT_CALL(settingParser,parse(_)).Times(0);
    try{
        subject.parse(root);
        FAIL();
    }
    catch(const std::runtime_error&){
        
    }
    cJSON_Delete(root);
    
    
    EXPECT_CALL(confParser,parse(_)).Times(0);
    EXPECT_CALL(ctrlParser,parse(_)).Times(0);
    EXPECT_CALL(settingParser,parse(_)).Times(0);
    EXPECT_CALL(deviceAbstractionParser,parse(_)).Times(0);
    root=cJSON_CreateObject();
    try{
        subject.parse(root);
        FAIL();
    }
    catch(const std::runtime_error&){}
    cJSON_Delete(root);
}

TEST(rootParser,configuration){
    cJSON* root=cJSON_CreateObject();
    cJSON* arr=cJSON_AddArrayToObject(root,"controllers");
    cJSON* arr2=cJSON_AddArrayToObject(root,"configurations");
    MockISettingsParser confParser,ctrlParser, settingParser, deviceAbstractionParser;
    RootParser subject(confParser,ctrlParser,settingParser,deviceAbstractionParser);
    EXPECT_CALL(ctrlParser,parse(arr)).Times(1);
    EXPECT_CALL(confParser,parse(arr2)).Times(1);
    EXPECT_CALL(settingParser,parse(_)).Times(0);
    EXPECT_CALL(deviceAbstractionParser,parse(_)).Times(0);
    try{
        subject.parse(root);
    }
    catch(const std::runtime_error&){
        FAIL();
    }
    cJSON_Delete(root);
}
TEST(rootParser,settings){
    cJSON* root=cJSON_CreateObject();
    cJSON* arr=cJSON_AddArrayToObject(root,"settings");
    cJSON* arr2=cJSON_AddArrayToObject(root,"controllers");
    cJSON* arr3= cJSON_AddArrayToObject(root,"configurations");
    MockISettingsParser confParser,ctrlParser, settingParser, deviceAbstractionParser;
    RootParser subject(confParser,ctrlParser,settingParser,deviceAbstractionParser);
    EXPECT_CALL(confParser,parse(arr3)).Times(1);
    EXPECT_CALL(ctrlParser,parse(arr2)).Times(1);
    EXPECT_CALL(settingParser,parse(arr)).Times(1);
    EXPECT_CALL(deviceAbstractionParser,parse(_)).Times(0);
    try{
        subject.parse(root);
    }
    catch(const std::runtime_error&){
        FAIL();
    }
    cJSON_Delete(root);
}
TEST(rootParser,deviceAbstraction){
    cJSON* root=cJSON_CreateObject();
    cJSON* arr=cJSON_AddArrayToObject(root,"settings");
    cJSON* arr2=cJSON_AddArrayToObject(root,"controllers");
    cJSON* arr3= cJSON_AddArrayToObject(root,"configurations");
    cJSON* arr4= cJSON_AddArrayToObject(root,"deviceAbstraction");
    MockISettingsParser confParser,ctrlParser, settingParser, deviceAbstractionParser;
    RootParser subject(confParser,ctrlParser,settingParser,deviceAbstractionParser);
    EXPECT_CALL(confParser,parse(arr3)).Times(1);
    EXPECT_CALL(ctrlParser,parse(arr2)).Times(1);
    EXPECT_CALL(settingParser,parse(arr)).Times(1);
    EXPECT_CALL(deviceAbstractionParser,parse(arr4)).Times(1);
    try{
        subject.parse(root);
    }
    catch(const std::runtime_error&){
        FAIL();
    }
    cJSON_Delete(root);
}

