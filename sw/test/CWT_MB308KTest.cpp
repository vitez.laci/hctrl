#include <gtest/gtest.h>
#include "models/CWT_MB308K.h"
#include "MockModbusController.h"
#include <memory>


using namespace std;
using namespace testing;

TEST(CWT_MB308K, Inputs){
    std::shared_ptr<ModbusController> ctrl1(new MockModbusController());
    CWT_MB308K subject(ctrl1,"A",15);
    for(int i=0;i<16;++i){
        subject.getInputPort(i);
    }
    try{
        subject.getInputPort(16);
        FAIL();
    }
    catch(out_of_range&){
        return;
    }
    FAIL();
}
TEST(CWT_MB308K, Outputs){
    std::shared_ptr<ModbusController> ctrl1(new MockModbusController());
    CWT_MB308K subject(ctrl1,"A",15);
    for(int i=0;i<12;++i){
        subject.getOutputPort(i);
    }
    try{
        subject.getOutputPort(12);
        FAIL();
    }
    catch(out_of_range&){
        return;
    }
    FAIL();
}
