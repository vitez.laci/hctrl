#include <gtest/gtest.h>
#include "settings/parsers/rootDeviceParser.h"
#include "resourceManager.h"
#include <MockResourceManager.h>
#include "models/modbusDevice.h"
#include <MockParserManager.h>
#include <MockDeviceParser.h>
#include "MockModbusController.h"

using namespace std;
using namespace testing;


TEST(RootDeviceParser,valid){
    MockParserManager modelParserManager;
    std::shared_ptr<MockDeviceParser> deviceParser1(new MockDeviceParser());
    std::shared_ptr<MockDeviceParser> deviceParser2(new MockDeviceParser());
    std::shared_ptr<ModbusController> ctrl1(new MockModbusController());
    cJSON* root=cJSON_CreateArray();
    cJSON* obj1root=cJSON_CreateObject();
    cJSON* obj2root=cJSON_CreateObject();
    cJSON_AddItemToArray(root,obj1root);
    cJSON_AddItemToArray(root,obj2root);
    cJSON_AddStringToObject(obj1root,"type","A");
    cJSON_AddStringToObject(obj2root,"type","B");

    RootDeviceParser subject;
    EXPECT_CALL(modelParserManager,createDeviceParser(StrEq("A")))
        .Times(1)
        .WillOnce(Return(deviceParser1));
    EXPECT_CALL(*deviceParser1,parse(obj1root,ctrl1));
    EXPECT_CALL(modelParserManager,createDeviceParser(StrEq("B")))
        .Times(1)
        .WillOnce(Return(deviceParser2));
    EXPECT_CALL(*deviceParser2,parse(obj2root,ctrl1));
    subject.parse(root,ctrl1);
}
TEST(RootDeviceParser,missingType){
    std::shared_ptr<ModbusController> ctrl1(new MockModbusController());
    cJSON* root=cJSON_CreateArray();
    cJSON* obj1root=cJSON_CreateObject();
    cJSON_AddItemToArray(root,obj1root);
    cJSON_AddStringToObject(obj1root,"type-","A");
    
    RootDeviceParser subject;
    try{
        subject.parse(root,ctrl1);
        FAIL();
    }
    catch(runtime_error&){}
}
TEST(RootDeviceParser,notAnArray){
    std::shared_ptr<ModbusController> ctrl1(new MockModbusController());
    cJSON* root=cJSON_CreateObject();
    cJSON_AddStringToObject(root,"type","A");
    
    RootDeviceParser subject;
    try{
        subject.parse(root,ctrl1);
        FAIL();
    }
    catch(runtime_error&){}
}
