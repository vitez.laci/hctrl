#include <gtest/gtest.h>
#include <chrono>
#include <cJSON.h>
#include "settings/parsers/SettingsParser.h"
#include "MockResourceManager.h"

using namespace std;
using namespace testing;


TEST(SystemSetting,valid){
    cJSON* root=cJSON_CreateObject();
    cJSON_AddNumberToObject(root,"mainLoopPeriodInMs",1523);
    SettingsParser subject;
    MockResourceManager resouceManager;
    EXPECT_CALL(resouceManager,setMainLoopPeriod(1523ms));
    subject.parse(root);
}
