#include <gmock/gmock.h>
#include "settings/parsers/LimitSwitchParser.h"
#include "MockIOHandler.h"
#include "MockInputPort.h"
#include "MockResourceManager.h"
#include "MockLimitSwitch.h"
#include <cJSON.h>
#include <memory>
#include <limits>


using namespace std;
using namespace testing;
class LSWP4T : public LimitSwitchParser{
public:
    virtual void doAdditionalParameterParsing(cJSON * root) override{LimitSwitchParser::doAdditionalParameterParsing(root);}
    virtual void registerInstance(std::shared_ptr<LimitSwitch> instance)override{LimitSwitchParser::registerInstance(instance);}
    virtual std::shared_ptr<LimitSwitch> createMappedDevice(std::shared_ptr<IIOHandler> ioHandler)override{return LimitSwitchParser::createMappedDevice(ioHandler);};
    auto getBehavior(){return behavior;}
};

TEST(LimitSwitchParser,doAdditionalParameterParsing_valid){
    LSWP4T subject;
    cJSON* root=cJSON_CreateObject();
    cJSON_AddStringToObject(root,"behavior","trueOnEndpoint");
    
    try{
        subject.doAdditionalParameterParsing(root);
        EXPECT_EQ(subject.getBehavior(),LimitSwitch::Behavior::trueOnEndpoint);
    }
    catch(runtime_error&){
        FAIL();
    }
}

TEST(LimitSwitchParser,invalidBehavior){
    LSWP4T subject;
    cJSON* root=cJSON_CreateObject();
    cJSON_AddStringToObject(root,"behavior-","trueOnEndpoint");
    try{
        subject.doAdditionalParameterParsing(root);
        FAIL();
    }
    catch(runtime_error&){
    }
    cJSON_AddStringToObject(root,"behavior","-trueOnEndpoint");
    try{
        subject.doAdditionalParameterParsing(root);
        FAIL();
    }
    catch(runtime_error&){
    }
}

TEST(LimitSwitchParser,createMappedDevice){
    MockInputPort<bool> inputPort;
    shared_ptr<MockIOHandler> ioHandler(new MockIOHandler());
    LSWP4T subject;
    cJSON* root=cJSON_CreateObject();
    cJSON_AddStringToObject(root,"behavior","trueOnEndpoint");
    
    EXPECT_CALL(*ioHandler,getInputPort(_)).Times(1).WillOnce(ReturnRef(inputPort));
    subject.createMappedDevice(ioHandler);

}
TEST(LimitSwitchParser,registerInstance){
    MockResourceManager resourceManager;
    std::shared_ptr<MockLimitSwith> limitSwitch(new MockLimitSwith());
    LSWP4T subject;
    cJSON* root=cJSON_CreateObject();
    cJSON_AddStringToObject(root,"behavior","trueOnEndpoint");
    
    
    EXPECT_CALL(resourceManager,registerLimitSwitch(
        _,
        Pointer(limitSwitch.get())))
    .Times(1);
    subject.registerInstance(limitSwitch);
}
