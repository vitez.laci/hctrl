#include <gmock/gmock.h>
#include <cJSON.h>
#include "settings/parsers/IOHandlerParser.h"
#include "MockMappingParser.h"
#include "MockIOHandler.h"
#include "MockResourceManager.h"
#include "MockModbusController.h"

using namespace std;
using namespace testing;
class IOHNDLR4T:public IOHandlerParser {
public:
    MOCK_METHOD(void ,registerIOhandler,(std::shared_ptr<ModbusController>),(override));
    MOCK_METHOD(MappingParser&,getMappingParser,(),(override));
};


TEST(IOHandlerParserTest, valid){
    cJSON* root=cJSON_CreateObject();
    std::shared_ptr<ModbusController> ctrl1(new MockModbusController());
    std::shared_ptr<IIOHandler>ioHandler(new MockIOHandler ());
    MockMappingParser mappingParser;
    MockResourceManager resourceManager;
    
    cJSON_AddStringToObject(root,"type","tp");
    cJSON_AddStringToObject(root,"name","nm");
    cJSON_AddNumberToObject(root,"address",584);
    cJSON* mappingRoot=cJSON_AddObjectToObject(root,"mapping");
    
    IOHNDLR4T subject;
    
    InSequence s;
    
    EXPECT_CALL(subject,registerIOhandler(Pointer(ctrl1.get()))).Times(1);
    EXPECT_CALL(subject,getMappingParser).Times(1).WillOnce(ReturnRef(mappingParser));
    EXPECT_CALL(resourceManager, getIOHandler(StrEq("nm"))).Times(1).WillOnce(Return(ioHandler));
    EXPECT_CALL(mappingParser,parse(mappingRoot,ioHandler)).Times(1);
    
    subject.parse(root,ctrl1);
}
TEST(IOHandlerParserTest, missingOrInvalidMapping){
    cJSON* root=cJSON_CreateObject();
    std::shared_ptr<ModbusController> ctrl1(new MockModbusController());
    std::shared_ptr<IIOHandler>ioHandler(new MockIOHandler ());
    MockMappingParser mappingParser;
    MockResourceManager resourceManager;
    
    cJSON_AddStringToObject(root,"type","tp");
    cJSON_AddStringToObject(root,"name","nm");
    cJSON_AddNumberToObject(root,"address",584);
    
    IOHNDLR4T subject;

    try{
        subject.parse(root,ctrl1);
        FAIL();
    }
    catch(runtime_error&){}
    cJSON_AddNumberToObject(root,"mapping",5);
    try{
        subject.parse(root,ctrl1);
        FAIL();
    }
    catch(runtime_error&){}
}
