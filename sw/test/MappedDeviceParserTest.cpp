#include <gmock/gmock.h>
#include "MockIOHandler.h"
#include <cJSON.h>
#include <memory>
#include <limits>
#include "settings/parsers/MappedDeviceParser.h"


using namespace std;
using namespace testing;

class MDP4T: public MappedDeviceParser{
public:
    uint16_t getPort(){return port;}
    const char* getDisplayName(){return displayName;}
    const char* getType(){return type;}
};

TEST(mappedDeviceParser,valid){
    shared_ptr<MockIOHandler> ioHandler(new MockIOHandler());
    MDP4T subject;
    cJSON* root=cJSON_CreateObject();
    cJSON_AddNumberToObject(root,"port",5);
    cJSON_AddStringToObject(root,"type","LimitSwitch");
    cJSON_AddStringToObject(root,"displayName","something");
    
    
    subject.parse(root,ioHandler);
    
    EXPECT_STREQ("LimitSwitch",subject.getType());
    EXPECT_STREQ("something",subject.getDisplayName());
    EXPECT_EQ(5,subject.getPort());
}

TEST(mappedDeviceParser,invalidPort){

    shared_ptr<MockIOHandler> ioHandler(new MockIOHandler());
    MDP4T subject;
    cJSON* root=cJSON_CreateObject();
    cJSON_AddNumberToObject(root,"port",-5);
    cJSON_AddStringToObject(root,"type","LimitSwitch");
    cJSON_AddStringToObject(root,"displayName","something");
    EXPECT_CALL(*ioHandler,getInputPort(_)).Times(0);
    try{
        subject.parse(root,ioHandler);
        FAIL();
    }
    catch(runtime_error&){
        return;
    }
    FAIL();
    cJSON_SetNumberValue(cJSON_GetObjectItem(root,"port"),numeric_limits<uint16_t>::max()+1);
    try{
        subject.parse(root,ioHandler);
        FAIL();
    }
    catch(runtime_error&){
        return;
    }
    FAIL();
}
TEST(mappedDeviceParser,notAnObjectRoot){
    shared_ptr<MockIOHandler> ioHandler(new MockIOHandler());
    MDP4T subject;
    cJSON* root=cJSON_CreateArray();
    try{
        subject.parse(root,ioHandler);
        FAIL();
    }
    catch(runtime_error&){
        return;
    }
    
}
