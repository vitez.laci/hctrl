#include <gtest/gtest.h>
#include "settings/parsers/parserHelper.h"

using namespace std;

TEST(ParserHelperTest, validParsing){
    cJSON* root=cJSON_CreateObject();
    cJSON_AddStringToObject(root,"key","val");
    cJSON_AddNumberToObject(root,"key2",2.3);
    ParserHelper subject(root);
    const char* val;
    subject.tryStringParse("key",&val,"");
    EXPECT_STREQ(val,"val");
    
    double vNum=subject.tryNumericParse("key2","");
    EXPECT_LE(vNum-2.3,1E-5);
    cJSON_Delete(root);
}
TEST(ParserHelperTest, missingKeysAndExceptionMessage){
    cJSON* root=cJSON_CreateObject();
    cJSON_AddStringToObject(root,"key","val");
    cJSON_AddNumberToObject(root,"key2",2.3);
    ParserHelper subject(root);
    const char* val;
    try{
        subject.tryStringParse("kM1",&val,"error message");
        FAIL();
    }
    catch(runtime_error&e){
        EXPECT_TRUE(string(e.what()).find("error message")!=string::npos);
    }
    try{
        subject.tryNumericParse("kM2", "error message2");
        FAIL();
    }
    catch(runtime_error&e){
        EXPECT_TRUE(string(e.what()).find("error message2")!=string::npos);
    }
    
    cJSON_Delete(root);
}
TEST(ParserHelperTest, typeError){
    cJSON* root=cJSON_CreateObject();
    cJSON_AddStringToObject(root,"key","val");
    cJSON_AddNumberToObject(root,"key2",2.3);
    ParserHelper subject(root);
    const char* val;
    try{
        subject.tryStringParse("key2",&val,"error message");
        FAIL();
    }
    catch(runtime_error&){}
    try{
        subject.tryNumericParse("Key","error message2");
        FAIL();
    }
    catch(runtime_error&){}
    
    cJSON_Delete(root);
}
TEST(ParserHelperTest, nullRoot){
    ParserHelper subject(NULL);
    const char* val;
    try{
        subject.tryStringParse("key2",&val,"error message");
        FAIL();
    }
    catch(runtime_error&){}
    try{
        subject.tryNumericParse("Key", "error message2");
        FAIL();
    }
    catch(runtime_error&){}
}
TEST(ParserHelperTest, arrayRoot){
    cJSON* root=cJSON_CreateArray();
    ParserHelper subject(root);
    const char* val;
    try{
        subject.tryStringParse("key2",&val,"error message");
        FAIL();
    }
    catch(runtime_error&){}
    try{
        subject.tryNumericParse("Key", "error message2");
        FAIL();
    }
    catch(runtime_error&){}
    cJSON_Delete(root);
}
TEST(ParserHelperTest, rawEnum){
    enum class TestEnum{A,B};
    cJSON* root=cJSON_CreateArray();
    ParserHelper subject(root);
    cJSON_AddStringToObject(root,"enum","A");
    EXPECT_EQ(subject.tryParseEnum<TestEnum>("enum","exc."),TestEnum::A);
    cJSON_SetValuestring(cJSON_GetObjectItem(root,"enum"),"B");
    EXPECT_EQ(subject.tryParseEnum<TestEnum>("enum","exc."),TestEnum::B);
    
    cJSON_SetValuestring(cJSON_GetObjectItem(root,"enum"),"C");
    try{
        subject.tryParseEnum<TestEnum>("enum","exc.");
        FAIL();
    }
    catch(runtime_error&){}
}
