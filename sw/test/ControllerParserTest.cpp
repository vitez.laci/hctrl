#include <gtest/gtest.h>
#include <cJSON.h>
#include <settings/parsers/controllerParser.h>
#include "MockModbusParser.h"
#include "MockResourceManager.h"
#include "resourceManager.h"
#include <functional>
#include "MockModbusController.h"
#include <iostream>

TEST(ControllerParser,PassesToModbusControllerParser){
    MockModbusParser modbusParser;
    MockResourceManager resourceManager;
    std::shared_ptr<ModbusController> ctrl1(new MockModbusController()), ctrl2(new MockModbusController());
    ControllerParser subject(modbusParser);
    cJSON* root=cJSON_CreateArray();
    cJSON* item=cJSON_CreateObject();
    cJSON* subItem=cJSON_CreateObject();
    cJSON* item2=cJSON_CreateObject();
    cJSON* subItem2=cJSON_CreateObject();
    
    cJSON_AddStringToObject(subItem,"type",ModbusController::typeName);
    cJSON_AddStringToObject(subItem2,"type",ModbusController::typeName);
    cJSON_AddItemToObject(item,"A",subItem);
    cJSON_AddItemToObject(item2,"B",subItem2);
    cJSON_AddItemToArray(root,item);
    cJSON_AddItemToArray(root,item2);
    
    EXPECT_CALL(modbusParser,parse(subItem)).Times(1);
    EXPECT_CALL(modbusParser,parse(subItem2)).Times(1);
    EXPECT_CALL(modbusParser,getInstance()).Times(2)
        .WillOnce(testing::Return(ctrl1))
        .WillOnce(testing::Return(ctrl2));
    EXPECT_CALL(resourceManager,registerModbusController(testing::StrEq("A"),testing::Pointer(ctrl1.get()))).Times(1);
    EXPECT_CALL(resourceManager,registerModbusController(testing::StrEq("B"),testing::Pointer(ctrl2.get()))).Times(1);
    subject.parse(root);
    
    cJSON_Delete(root);
    
}

TEST(ControllerParser,notAnArray){
    MockModbusParser modbusParser;
    MockResourceManager resourceManager;
    ControllerParser subject(modbusParser);
    cJSON* item=cJSON_CreateObject();
    try{
        subject.parse(item);
        FAIL();
    }
    catch(std::runtime_error&){}
    
}
TEST(ControllerParser,notASubobject){
    MockModbusParser modbusParser;
    MockResourceManager resourceManager;
    ControllerParser subject(modbusParser);
    cJSON* root=cJSON_CreateArray();
    cJSON* item=cJSON_CreateObject();
    cJSON_AddItemToArray(root,item);
    try{
        subject.parse(root);
        FAIL();
    }
    catch(std::runtime_error&){
    }
    
}
TEST(ControllerParser,notATypedObject){
    MockModbusParser modbusParser;
    MockResourceManager resourceManager;
    ControllerParser subject(modbusParser);
    cJSON* root=cJSON_CreateArray();
    cJSON* item=cJSON_CreateObject();
    cJSON* subItem=cJSON_CreateObject();
    cJSON_AddItemToObject(item,"A",subItem);
    cJSON_AddItemToArray(root,item);
    try{
        subject.parse(root);
        FAIL();
    }
    catch(std::runtime_error&){
    }
    
}
