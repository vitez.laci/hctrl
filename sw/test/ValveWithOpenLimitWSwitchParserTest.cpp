#include <gtest/gtest.h>
#include "settings/parsers/ValveWithOpenLimitSwitchParser.h"
#include <cJSON.h>
#include <memory>
#include "MockResourceManager.h"
#include "MockLimitSwitch.h"
#include "MockMonostabilValve.h"
#include "MockValveErrorDetector.h"
#include "MockValveWihOpenSwitch.h"

using namespace std;
using namespace testing;

class VWOLSWP3T:public ValveWithOpenLimitSwitchParser{
public:
    ResourceManager::LimitSwitchRole getInputRole(){return inputRole;}
    ResourceManager::MonostabilValveRole getOutputRole(){return outputRole;}
    std::chrono::milliseconds getOperationErrorThreshold(){return operationErrorThreshold;}
    void doAdditionalParameterParsing(cJSON * root) override {ValveWithOpenLimitSwitchParser::doAdditionalParameterParsing(root);};
    std::shared_ptr<myDeviceType> createAbstractedDevice() override{return ValveWithOpenLimitSwitchParser::createAbstractedDevice();};
    void registerInstance(std::shared_ptr<myDeviceType> instance) override{ValveWithOpenLimitSwitchParser::registerInstance(instance);};
};

TEST(ValveWithOpenLimitSwitchTest,validParsing){
    cJSON* root=cJSON_CreateObject();
    cJSON_AddStringToObject(root,"inputRole","DisinfectionBypassValveFeedback");
    cJSON_AddStringToObject(root,"outputRole","DisinfectionBypassValveControl");
    cJSON_AddNumberToObject(root,"timeLimitInMs",60000);
    
    VWOLSWP3T subject;
    subject.doAdditionalParameterParsing(root);
    EXPECT_EQ(subject.getInputRole(),ResourceManager::LimitSwitchRole::DisinfectionBypassValveFeedback);
    EXPECT_EQ(subject.getOutputRole(),ResourceManager::MonostabilValveRole::DisinfectionBypassValveControl);
    EXPECT_EQ(subject.getOperationErrorThreshold(),60000ms);
}
TEST(ValveWithOpenLimitSwitchTest,validCreation){
    cJSON* root=cJSON_CreateObject();
    cJSON_AddStringToObject(root,"inputRole","DisinfectionBypassValveFeedback");
    cJSON_AddStringToObject(root,"outputRole","DisinfectionBypassValveControl");
    cJSON_AddNumberToObject(root,"timeLimitInMs",60000);
    MockResourceManager resourceManager;
    VWOLSWP3T subject;
    shared_ptr<MockLimitSwith>limitSwitch(new MockLimitSwith());
    shared_ptr<MockMonostabilValve>monostabilValve(new MockMonostabilValve());
    subject.doAdditionalParameterParsing(root);
    
    
    EXPECT_CALL(resourceManager,getLimitSwitch(ResourceManager::LimitSwitchRole::DisinfectionBypassValveFeedback)).Times(1).WillOnce(Return(limitSwitch));
    EXPECT_CALL(resourceManager,getMonostabilValve(ResourceManager::MonostabilValveRole::DisinfectionBypassValveControl)).Times(1).WillOnce(Return(monostabilValve));
    subject.createAbstractedDevice();
    
}

TEST(ValveWithOpenLimitSwitchTest,registration){
    
    MockResourceManager resourceManager;
    VWOLSWP3T subject;
    shared_ptr<MockValveWithOpenLimitSwitch> device(new MockValveWithOpenLimitSwitch());

    EXPECT_CALL(resourceManager,registerValveWithOpenLimitSwitch(_,Pointer(device.get()))).Times(1);
    subject.registerInstance(device);
    
}
