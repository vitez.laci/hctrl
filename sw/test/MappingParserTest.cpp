#include <gtest/gtest.h>
#include "MockIOHandler.h"
#include "settings/parsers/mappingParser.h"
#include <cJSON.h>
#include <memory>
#include "MockParserManager.h"
#include "MockMappedDeviceParser.h"

using namespace std;
using namespace testing;

TEST(MappingParser, valid){
    shared_ptr<IIOHandler> ioHandler(new MockIOHandler);
    MockParserManager parserManager;
    shared_ptr<MockMappedDeviceParser> mappedDeviceParser1(new MockMappedDeviceParser);
    shared_ptr<MockMappedDeviceParser> mappedDeviceParser2(new MockMappedDeviceParser);
    
    cJSON* root=cJSON_CreateObject();
    cJSON* input=cJSON_AddArrayToObject(root,"input");
    cJSON* output=cJSON_AddArrayToObject(root,"output");
    cJSON* inObj=cJSON_CreateObject();
    cJSON* outObj=cJSON_CreateObject();
    cJSON_AddStringToObject(inObj,"type","A");
    cJSON_AddStringToObject(outObj,"type","B");
    cJSON_AddItemToArray(input,inObj);
    cJSON_AddItemToArray(output,outObj);
    
    MappingParser subject;
    EXPECT_CALL(parserManager,createMappedDeviceParser(StrEq("A"))).Times(1).WillOnce(Return(mappedDeviceParser1));
    EXPECT_CALL(parserManager,createMappedDeviceParser(StrEq("B"))).Times(1).WillOnce(Return(mappedDeviceParser2));
    EXPECT_CALL(*mappedDeviceParser1,parse(inObj,Pointer(ioHandler.get()))).Times(1);
    EXPECT_CALL(*mappedDeviceParser2,parse(outObj,Pointer(ioHandler.get()))).Times(1);
    
    subject.parse(root, ioHandler);
}
TEST(MappingParser, notype){
    shared_ptr<IIOHandler> ioHandler(new MockIOHandler);
    MockParserManager parserManager;
    shared_ptr<MockMappedDeviceParser> mappedDeviceParser1(new MockMappedDeviceParser);
    shared_ptr<MockMappedDeviceParser> mappedDeviceParser2(new MockMappedDeviceParser);
    
    cJSON* root=cJSON_CreateObject();
    cJSON* input=cJSON_AddArrayToObject(root,"input");
    cJSON* output=cJSON_AddArrayToObject(root,"output");
    cJSON* inObj=cJSON_CreateObject();
    cJSON* outObj=cJSON_CreateObject();
    cJSON_AddStringToObject(inObj,"typ","A");
    cJSON_AddStringToObject(outObj,"typ","B");
    cJSON_AddItemToArray(input,inObj);
    cJSON_AddItemToArray(output,outObj);
    
    MappingParser subject;
    try{    
        subject.parse(root, ioHandler);
        FAIL();
    }
    catch(runtime_error&){}
}
