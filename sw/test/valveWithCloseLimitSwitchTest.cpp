#include <memory>
#include <gtest/gtest.h>
#include "framework/valveWithCloseLimitSwitch.h"
#include "MockLimitSwitch.h"
#include "MockValveErrorDetector.h"
#include "MockI2wayValve.h"

using namespace std;
using testing::Return;

TEST(ValveWithCloseLimitSwitch,open){
    auto valve=make_shared<MockI2WayValve>();
    shared_ptr<MockLimitSwith> limitSwitch(new MockLimitSwith());
    shared_ptr<MockValveErrorDetector> valveErrorDetector(new MockValveErrorDetector());
    ValveWithCloseLimitSwitch subject(valve,limitSwitch,valveErrorDetector);
    EXPECT_CALL(*valve,open()).Times(1);
    EXPECT_CALL(*valveErrorDetector,triggerAction()).Times(0);
    subject.open();
}
TEST(ValveWithCloseLimitSwitch,close){
    auto valve=make_shared<MockI2WayValve>();
    shared_ptr<MockLimitSwith> limitSwitch(new MockLimitSwith());
    shared_ptr<MockValveErrorDetector> valveErrorDetector(new MockValveErrorDetector());
    ValveWithCloseLimitSwitch subject(valve,limitSwitch,valveErrorDetector);
    EXPECT_CALL(*valve,close()).Times(1);
    EXPECT_CALL(*valveErrorDetector,triggerAction()).Times(1);
    subject.close();
}
TEST(ValveWithCloseLimitSwitch,isOpen){
    auto valve=make_shared<MockI2WayValve>();
    shared_ptr<MockLimitSwith> limitSwitch(new MockLimitSwith());
    shared_ptr<MockValveErrorDetector> valveErrorDetector(new MockValveErrorDetector());
    ValveWithCloseLimitSwitch subject(valve,limitSwitch,valveErrorDetector);
    EXPECT_CALL(*limitSwitch,getState())
            .Times(2)
            .WillOnce(Return(LimitSwitch::State::onEndpoint))
            .WillOnce(Return(LimitSwitch::State::notOnEndpoint))
            ;
    EXPECT_CALL(*valveErrorDetector,approveAction()).Times(1);
    EXPECT_TRUE(subject.isClosed());
    EXPECT_CALL(*valveErrorDetector,approveAction()).Times(0);
    EXPECT_FALSE(subject.isClosed());
}
TEST(ValveWithCloseLimitSwitch,hasError){
    auto valve=make_shared<MockI2WayValve>();
    shared_ptr<MockLimitSwith> limitSwitch(new MockLimitSwith());
    shared_ptr<MockValveErrorDetector> valveErrorDetector(new MockValveErrorDetector());
    ValveWithCloseLimitSwitch subject(valve,limitSwitch,valveErrorDetector);
    EXPECT_CALL(*valveErrorDetector,hasError())
    .Times(2)
    .WillOnce(Return(true))
    .WillOnce(Return(false));
    EXPECT_TRUE(subject.hasError());
    EXPECT_FALSE(subject.hasError());
}


