#include <gmock/gmock.h>
#include <cJSON.h>
#include "settings/parsers/SimpleMappedDeviceParser.h"
#include "MockIOHandler.h"
#include <memory>

using namespace std;
using namespace testing;

enum class TestKey{A,B};
class TestClass{};
class SMDPTI4T: public SimpleMappedDeviceParser<TestKey,TestClass>{
public:
    MOCK_METHOD(void, doAdditionalParameterParsing,(cJSON * root),(override));
    MOCK_METHOD(void,registerInstance,(std::shared_ptr<TestClass>),(override));
    MOCK_METHOD(std::shared_ptr<TestClass>,createMappedDevice,(std::shared_ptr<IIOHandler> ioHandler),(override));
};

TEST(SimpleMappedDeviceParser,valid){
    SMDPTI4T subject;
    shared_ptr<MockIOHandler> ioHandler(new MockIOHandler());
    shared_ptr<TestClass> testClass(new TestClass());
    cJSON* root=cJSON_CreateObject();
    cJSON_AddNumberToObject(root,"port",5);
    cJSON_AddStringToObject(root,"type","LimitSwitch");
    cJSON_AddStringToObject(root,"role","A");
    cJSON_AddStringToObject(root,"displayName","something");
    
    InSequence s;
    EXPECT_CALL(subject,doAdditionalParameterParsing(root)).Times(1);
    EXPECT_CALL(subject,createMappedDevice(Pointer(ioHandler.get()))).Times(1).WillOnce(Return(testClass));
    EXPECT_CALL(subject,registerInstance(Pointer(testClass.get()))).Times(1);
    
    subject.parse(root,ioHandler);
}
