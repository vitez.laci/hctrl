#include <gtest/gtest.h>

#include "settings/parsers/MonostabilValveParser.h"
#include "MockResourceManager.h"
#include "MockIOHandler.h"
#include "MockOutputPort.h"
#include <cJSON.h>
#include <memory>


using namespace std;
using namespace testing;

TEST(MonostabilValve, valid){
    
    shared_ptr<MockIOHandler> ioHandler(new MockIOHandler());
    MonostabilValveParser subject;
    MockResourceManager resourceManager;
    MockOutputPort<bool> outputPort;
    cJSON* root=cJSON_CreateObject();
    cJSON_AddNumberToObject(root,"port",5);
    cJSON_AddStringToObject(root,"type","LimitSwitch");
    cJSON_AddStringToObject(root,"role","DisinfectionBypassValveControl");
    cJSON_AddStringToObject(root,"displayName","something");
    cJSON_AddStringToObject(root,"defaultState","open");
    EXPECT_CALL(*ioHandler,getOutputPort(5)).Times(1).WillOnce(ReturnRef(outputPort));
    EXPECT_CALL(resourceManager,registerMonostabilValve(ResourceManager::MonostabilValveRole::DisinfectionBypassValveControl,_));
    
    subject.parse(root,ioHandler);
}
