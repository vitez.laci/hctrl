#include <gmock/gmock.h>
#include <cJSON.h>
#include "MockParserManager.h"
#include "settings/parsers/DeviceAbstractionParser.h"
#include <memory>

using namespace std;
using namespace testing;


enum class k{A,B};
class TestClass{};
class DAP4T: public DeviceAbstractionParser<k,TestClass>{
public:
    MOCK_METHOD(void ,registerInstance,(std::shared_ptr<myDeviceType>),(override));
    MOCK_METHOD(std::shared_ptr<myDeviceType> ,createAbstractedDevice,(),(override));
    MOCK_METHOD(void ,doAdditionalParameterParsing,(cJSON* root) ,(override));
    
};

TEST(DeviceAbstractionParserTest,valid){
    DAP4T subject;
    shared_ptr<TestClass> testClass(new TestClass());
    cJSON* root=cJSON_CreateObject();
    cJSON_AddStringToObject(root,"type","stg");
    cJSON_AddStringToObject(root,"role","A");
    
    InSequence s;
    EXPECT_CALL(subject,doAdditionalParameterParsing(root)).Times(1);
    EXPECT_CALL(subject,createAbstractedDevice()).Times(1).WillOnce(Return(testClass));
    EXPECT_CALL(subject,registerInstance(Pointer(testClass.get()))).Times(1);
    
    subject.parse(root);
}

