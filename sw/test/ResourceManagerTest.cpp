#include <gtest/gtest.h>
#include <resourceManager.h>
#include <memory>
#include "models/IThermostat.h"
#include "MockIThermostat.h"
#include "models/IIOHandler.h"
#include "MockIOHandler.h"
#include "MockModbusController.h"
#include "MockLimitSwitch.h"
#include "MockModbusDevice.h"

using namespace std;
using namespace testing;
class RMTI:public ResourceManager{
public:
    RMTI();
    RMTI& create(){return *this;}
};
RMTI::RMTI()
{
    instanceCreator=std::bind(&RMTI::create,this);
}

TEST(ResourceManager,registerControllerOnlyOnceAndGet){
    shared_ptr<ModbusController> controller1(new MockModbusController());
    RMTI resourceManager;
    ResourceManager::I().registerModbusController("a",controller1);
    try{
        ResourceManager::I().registerModbusController("a",controller1);    
        FAIL();
    }
    catch(runtime_error&){}
    EXPECT_EQ(ResourceManager::I().getModbusController("a").get(),controller1.get());
}
TEST(ResourceManager,registerThermostatOnlyOnceAndGet){
    RMTI resourceManager;
    shared_ptr<IThermostat> thermostat=make_shared<MockIThermostat>();
    ResourceManager::I().registerThermostat("dev1",thermostat);
    try{
        ResourceManager::I().registerThermostat("dev1",thermostat);
        FAIL();
    }
    catch(runtime_error&){}
    EXPECT_EQ(ResourceManager::I().getThermostat("dev1").get(),thermostat.get());
}
TEST(ResourceManager,registerIOExpanderOnlyOnceAndGet){
    RMTI resourceManager;
    shared_ptr<IIOHandler> exp=make_shared<MockIOHandler>();
    ResourceManager::I().registerIOHandler("ioh1",exp);
    try{
        ResourceManager::I().registerIOHandler("ioh1",exp);
        FAIL();
    }
    catch(runtime_error&){}
    EXPECT_EQ(ResourceManager::I().getIOHandler("ioh1").get(),exp.get());
}
TEST(ResourceManager,registerLimitSwitchOnceAndGet){
    RMTI resourceManager;
    shared_ptr<LimitSwitch> ls=make_shared<MockLimitSwith>();
    ResourceManager::I().registerLimitSwitch(ResourceManager::LimitSwitchRole::DisinfectionBypassValveFeedback,ls);
    try{
        ResourceManager::I().registerLimitSwitch(ResourceManager::LimitSwitchRole::DisinfectionBypassValveFeedback,ls);
        FAIL();
    }
    catch(runtime_error&){}
    EXPECT_EQ(ResourceManager::I().getLimitSwitch(ResourceManager::LimitSwitchRole::DisinfectionBypassValveFeedback).get(),ls.get());
}
TEST(ResourceManager,updateAllModbusDeviceCache){
    RMTI resourceManager;
    shared_ptr<MockModbusDevice> dev1=make_shared<MockModbusDevice>();
    shared_ptr<MockModbusDevice> dev2=make_shared<MockModbusDevice>();
    resourceManager.registerAutoUpdatebleModbusDevice(dev1);
    resourceManager.registerAutoUpdatebleModbusDevice(dev2);
    EXPECT_CALL(*dev1,updateCache).Times(1);
    EXPECT_CALL(*dev2,updateCache).Times(1);
    resourceManager.updateModbusDeviceCaches();
}
