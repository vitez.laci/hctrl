#include <gtest/gtest.h>
#include "framework/simpleOutput.h"
#include "MockOutputPort.h"

TEST(SimpleOutput, write){
    testing::InSequence s;
    MockOutputPort<bool> output;
    SimpleOutput subject(output);
    
    EXPECT_CALL(output,write(true)).Times(1);
    EXPECT_CALL(output,write(false)).Times(1);
    
    subject.write(true);
    subject.write(false);
}
