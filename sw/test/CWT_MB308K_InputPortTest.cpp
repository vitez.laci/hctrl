#include <gtest/gtest.h>
#include "models/CWT_MB308K_InputPort.h"
#include "MockCWT_MB308K.h"


using namespace std;
using namespace testing;

TEST(CWT_MB308K_InputPort, PassesCallToDevice){
    MockCWT_MB308K device;
    CWT_MB308K_InputPort subject(&device,125);
    EXPECT_CALL(device,readInput(125)).Times(2)
    .WillOnce(Return(0)).WillOnce(Return(1));
    
    EXPECT_FALSE(subject.read());
    EXPECT_TRUE(subject.read());
}
