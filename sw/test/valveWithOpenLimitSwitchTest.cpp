#include <memory>
#include <gtest/gtest.h>
#include "framework/valveWithOpenLimitSwitch.h"
#include "MockLimitSwitch.h"
#include "MockValveErrorDetector.h"
#include "MockI2wayValve.h"

using namespace std;
using testing::Return;

TEST(ValveWithOpenLimitSwitch,open){
    auto valve=make_shared<MockI2WayValve>();
    shared_ptr<MockLimitSwith> limitSwitch(new MockLimitSwith());
    shared_ptr<MockValveErrorDetector> valveErrorDetector(new MockValveErrorDetector());
    ValveWithOpenLimitSwitch subject(valve,limitSwitch,valveErrorDetector);
    EXPECT_CALL(*valve,open()).Times(1);
    EXPECT_CALL(*valveErrorDetector,triggerAction()).Times(1);
    subject.open();
}
TEST(ValveWithOpenLimitSwitch,close){
    auto valve=make_shared<MockI2WayValve>();
    shared_ptr<MockLimitSwith> limitSwitch(new MockLimitSwith());
    shared_ptr<MockValveErrorDetector> valveErrorDetector(new MockValveErrorDetector());
    ValveWithOpenLimitSwitch subject(valve,limitSwitch,valveErrorDetector);
    EXPECT_CALL(*valve,close()).Times(1);
    EXPECT_CALL(*valveErrorDetector,triggerAction()).Times(0);
    subject.close();
}
TEST(ValveWithOpenLimitSwitch,isOpen){
    auto valve=make_shared<MockI2WayValve>();
    shared_ptr<MockLimitSwith> limitSwitch(new MockLimitSwith());
    shared_ptr<MockValveErrorDetector> valveErrorDetector(new MockValveErrorDetector());
    ValveWithOpenLimitSwitch subject(valve,limitSwitch,valveErrorDetector);
    EXPECT_CALL(*limitSwitch,getState())
            .Times(2)
            .WillOnce(Return(LimitSwitch::State::onEndpoint))
            .WillOnce(Return(LimitSwitch::State::notOnEndpoint))
            ;
    EXPECT_CALL(*valveErrorDetector,approveAction()).Times(1);
    EXPECT_TRUE(subject.isOpen());
    EXPECT_CALL(*valveErrorDetector,approveAction()).Times(0);
    EXPECT_FALSE(subject.isOpen());
}


