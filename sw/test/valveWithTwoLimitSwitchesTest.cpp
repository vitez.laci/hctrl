#include <memory>
#include <gtest/gtest.h>
#include "framework/valveWithTwoLimitSwitches.h"
#include "MockLimitSwitch.h"
#include "MockValveErrorDetector.h"
#include "MockI2wayValve.h"

using namespace std;
using testing::Return;

TEST(ValveWithTwoLimitSwitches,open){
    auto valve=make_shared<MockI2WayValve>();
    MockLimitSwith limitSwitchOpen;
    MockLimitSwith limitSwitchClose;
    shared_ptr<MockValveErrorDetector> valveErrorDetectorOpen(new MockValveErrorDetector());
    shared_ptr<MockValveErrorDetector> valveErrorDetectorClose(new MockValveErrorDetector());
    ValveWithTwoLimitSwitches subject(valve,limitSwitchOpen,valveErrorDetectorOpen,limitSwitchClose,valveErrorDetectorClose);
    EXPECT_CALL(*valve,open()).Times(1);
    EXPECT_CALL(*valveErrorDetectorOpen,triggerAction()).Times(1);
    EXPECT_CALL(*valveErrorDetectorClose,triggerAction()).Times(0);
    subject.open();
}
TEST(ValveWithTwoLimitSwitches,close){
    auto valve=make_shared<MockI2WayValve>();
    MockLimitSwith limitSwitchOpen;
    MockLimitSwith limitSwitchClose;
    shared_ptr<MockValveErrorDetector> valveErrorDetectorOpen(new MockValveErrorDetector());
    shared_ptr<MockValveErrorDetector> valveErrorDetectorClose(new MockValveErrorDetector());
    ValveWithTwoLimitSwitches subject(valve,limitSwitchOpen,valveErrorDetectorOpen,limitSwitchClose,valveErrorDetectorClose);
    EXPECT_CALL(*valve,close()).Times(1);
    EXPECT_CALL(*valveErrorDetectorOpen,triggerAction()).Times(0);
    EXPECT_CALL(*valveErrorDetectorClose,triggerAction()).Times(1);
    subject.close();
}
TEST(ValveWithTwoLimitSwitches,isOpen){
    auto valve=make_shared<MockI2WayValve>();
    MockLimitSwith limitSwitchOpen;
    MockLimitSwith limitSwitchClose;
    shared_ptr<MockValveErrorDetector> valveErrorDetectorOpen(new MockValveErrorDetector());
    shared_ptr<MockValveErrorDetector> valveErrorDetectorClose(new MockValveErrorDetector());
    ValveWithTwoLimitSwitches subject(valve,limitSwitchOpen,valveErrorDetectorOpen,limitSwitchClose,valveErrorDetectorClose);
    EXPECT_CALL(limitSwitchClose,getState()).Times(0);
    EXPECT_CALL(*valveErrorDetectorClose,approveAction()).Times(0);
    EXPECT_CALL(limitSwitchOpen,getState())
            .Times(2)
            .WillOnce(Return(LimitSwitch::State::onEndpoint))
            .WillOnce(Return(LimitSwitch::State::notOnEndpoint))
            ;
    EXPECT_CALL(*valveErrorDetectorOpen,approveAction()).Times(1);
    EXPECT_TRUE(subject.isOpen());
    EXPECT_CALL(*valveErrorDetectorOpen,approveAction()).Times(0);
    EXPECT_FALSE(subject.isOpen());
}
TEST(ValveWithTwoLimitSwitches,isClosed){
    auto valve=make_shared<MockI2WayValve>();
    MockLimitSwith limitSwitchOpen;
    MockLimitSwith limitSwitchClose;
    shared_ptr<MockValveErrorDetector> valveErrorDetectorOpen(new MockValveErrorDetector());
    shared_ptr<MockValveErrorDetector> valveErrorDetectorClose(new MockValveErrorDetector());
    ValveWithTwoLimitSwitches subject(valve,limitSwitchOpen,valveErrorDetectorOpen,limitSwitchClose,valveErrorDetectorClose);
    EXPECT_CALL(limitSwitchClose,getState()).Times(0);
    EXPECT_CALL(*valveErrorDetectorOpen,approveAction()).Times(0);
    EXPECT_CALL(limitSwitchClose,getState())
            .Times(2)
            .WillOnce(Return(LimitSwitch::State::onEndpoint))
            .WillOnce(Return(LimitSwitch::State::notOnEndpoint))
            ;
    EXPECT_CALL(*valveErrorDetectorClose,approveAction()).Times(1);
    EXPECT_TRUE(subject.isClosed());
    EXPECT_CALL(*valveErrorDetectorClose,approveAction()).Times(0);
    EXPECT_FALSE(subject.isClosed());
}
TEST(ValveWithTwoLimitSwitches,hasError){
    auto valve=make_shared<MockI2WayValve>();
    MockLimitSwith limitSwitchOpen;
    MockLimitSwith limitSwitchClose;
    shared_ptr<MockValveErrorDetector> valveErrorDetectorOpen(new MockValveErrorDetector());
    shared_ptr<MockValveErrorDetector> valveErrorDetectorClose(new MockValveErrorDetector());
    ValveWithTwoLimitSwitches subject(valve,limitSwitchOpen,valveErrorDetectorOpen,limitSwitchClose,valveErrorDetectorClose);
    EXPECT_CALL(*valveErrorDetectorOpen,hasError())
    .Times(4)
    .WillOnce(Return(false))
    .WillOnce(Return(false))
    .WillOnce(Return(true))
    .WillOnce(Return(true));
    EXPECT_CALL(*valveErrorDetectorClose,hasError())
    .Times(2)
    .WillOnce(Return(false))
    .WillOnce(Return(true));
    EXPECT_FALSE(subject.hasError());
    EXPECT_TRUE(subject.hasError());
    EXPECT_TRUE(subject.hasError());
    EXPECT_TRUE(subject.hasError());
}
