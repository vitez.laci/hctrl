#include <gtest/gtest.h>
#include <framework/valveErrorDetector.h>
#include <thread>

using namespace std;
TEST(valveErrorDetector,constuctionState){
    ValveErrorDetector subject(100ms);
    this_thread::sleep_for(200ms);
    ASSERT_FALSE(subject.hasError());
}
TEST(valveErrorDetector,triggerActionAndExpireTimer){
    ValveErrorDetector subject(100ms);
    subject.triggerAction();
    this_thread::sleep_for(50ms);
    ASSERT_FALSE(subject.hasError());
    this_thread::sleep_for(100ms);
    ASSERT_TRUE(subject.hasError());
}
TEST(valveErrorDetector,triggerActionApproveAndExpireTimer){
    ValveErrorDetector subject(100ms);
    subject.triggerAction();
    this_thread::sleep_for(50ms);
    ASSERT_FALSE(subject.hasError());
    subject.approveAction();
    this_thread::sleep_for(100ms);
    ASSERT_FALSE(subject.hasError());
}
