#include <gtest/gtest.h>
#include <framework/bistabilValve.h>
#include "MockOutputPort.h"

TEST(BistabilValve,open){
    testing::InSequence s;
    MockOutputPort<bool> openOutput,closeOutput;
    BistabilValve subject(openOutput,closeOutput);
    EXPECT_CALL(closeOutput,write(false)).Times(1);
    EXPECT_CALL(openOutput,write(true)).Times(1);
    subject.open();
}
TEST(BistabilValve,close){
    testing::InSequence s;
    MockOutputPort<bool> openOutput,closeOutput;
    BistabilValve subject(openOutput,closeOutput);
    EXPECT_CALL(openOutput,write(false)).Times(1);
    EXPECT_CALL(closeOutput,write(true)).Times(1);
    subject.close();
}
TEST(BistabilValve,stop){
    MockOutputPort<bool> openOutput,closeOutput;
    BistabilValve subject(openOutput,closeOutput);
    EXPECT_CALL(closeOutput,write(false)).Times(1);
    EXPECT_CALL(openOutput,write(false)).Times(1);
    subject.stop();
}
