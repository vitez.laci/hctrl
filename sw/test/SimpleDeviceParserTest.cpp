#include <gmock/gmock.h>
#include <cJSON.h>
#include <memory>
#include "MockModbusController.h"
#include "settings/parsers/simpleDeviceParser.h"

using namespace std;
using namespace testing;
class SDP4Test:public SimpleDeviceParser{
public:
    const char* getName();
    const char* getType();
    uint16_t getAddress();
    
};
uint16_t SDP4Test::getAddress()
{
    return address;
}
const char * SDP4Test::getName()
{
    return name;
}
const char * SDP4Test::getType()
{
    return type;
}


TEST(SimpleDeviceParser, valid){
    std::shared_ptr<ModbusController> ctrl1(new MockModbusController());
    cJSON* root=cJSON_CreateObject();
    SDP4Test subject;
    cJSON_AddStringToObject(root,"type","tp");
    cJSON_AddStringToObject(root,"name","nm");
    cJSON_AddNumberToObject(root,"address",584);
    
    
    subject.parse(root,ctrl1);
    EXPECT_STREQ(subject.getName(),"nm");
    EXPECT_STREQ(subject.getType(),"tp");
    EXPECT_EQ(subject.getAddress(),584);
}
TEST(SimpleDeviceParser, missingType){
    std::shared_ptr<ModbusController> ctrl1(new MockModbusController());
    cJSON* root=cJSON_CreateObject();
    SDP4Test subject;
    cJSON_AddStringToObject(root,"name","nm");
    cJSON_AddNumberToObject(root,"address",584);
    
    try{
        subject.parse(root,ctrl1);
        FAIL();
    }
    catch(runtime_error&){}
    
}
TEST(SimpleDeviceParser, missingName){
    std::shared_ptr<ModbusController> ctrl1(new MockModbusController());
    cJSON* root=cJSON_CreateObject();
    SDP4Test subject;
    
    cJSON_AddStringToObject(root,"type","tp");
    cJSON_AddNumberToObject(root,"address",584);
    
    try{
        subject.parse(root,ctrl1);
        FAIL();
    }
    catch(runtime_error&){}
    
}
TEST(SimpleDeviceParser, missingAddress){
    std::shared_ptr<ModbusController> ctrl1(new MockModbusController());
    cJSON* root=cJSON_CreateObject();
    SDP4Test subject;
    
    cJSON_AddStringToObject(root,"name","nm");
    cJSON_AddStringToObject(root,"type","tp");
    
    try{
        subject.parse(root,ctrl1);
        FAIL();
    }
    catch(runtime_error&){}
    
}
TEST(SimpleDeviceParser, invalidAddress){
    std::shared_ptr<ModbusController> ctrl1(new MockModbusController());
    cJSON* root=cJSON_CreateObject();
    SDP4Test subject;
    
    cJSON_AddStringToObject(root,"name","nm");
    cJSON_AddStringToObject(root,"type","tp");
    cJSON_AddNumberToObject(root,"address",-5);
    
    try{
        subject.parse(root,ctrl1);
        FAIL();
    }
    catch(runtime_error&){}
    
    cJSON_SetNumberValue(cJSON_GetObjectItem(root,"address"),65536);
    try{
        subject.parse(root,ctrl1);
        FAIL();
    }
    catch(runtime_error&){}
    
}

