#include <gmock/gmock.h>
#include "settings/parsers/RootDeviceAbstractionParser.h"
#include <cJSON.h>
#include "MockParserManager.h"
#include "MockDeviceAbstractionParser.h"
#include <memory>

using namespace std;
using namespace testing;


TEST(RootDeviceAbstractionParserTest,valid){
    
    MockParserManager parserManager; 
    cJSON* root=cJSON_CreateArray();
    cJSON* obj1root=cJSON_CreateObject();
    cJSON* obj2root=cJSON_CreateObject();
    cJSON_AddItemToArray(root,obj1root);
    cJSON_AddItemToArray(root,obj2root);
    cJSON_AddStringToObject(obj1root,"type","A");
    cJSON_AddStringToObject(obj2root,"type","B");
    RootDeviceAbstractionParser subject;
    shared_ptr<MockDeviceAbstractionParser> returnedParser1(new MockDeviceAbstractionParser());
    shared_ptr<MockDeviceAbstractionParser> returnedParser2(new MockDeviceAbstractionParser());
    
    EXPECT_CALL(parserManager,createDeviceAbstractionParser(StrEq("A"))).Times(1).WillOnce(Return(returnedParser1));
    EXPECT_CALL(parserManager,createDeviceAbstractionParser(StrEq("B"))).Times(1).WillOnce(Return(returnedParser2));
    EXPECT_CALL(*returnedParser1,parse(obj1root)).Times(1);
    EXPECT_CALL(*returnedParser2,parse(obj2root)).Times(1);
    subject.parse(root);
}

TEST(RootDeviceAbstractionParserTest,missingType){
    cJSON* root=cJSON_CreateArray();
    cJSON* obj1root=cJSON_CreateObject();
    cJSON_AddItemToArray(root,obj1root);
    cJSON_AddStringToObject(obj1root,"type-","A");
    
    RootDeviceAbstractionParser subject;
    try{
        subject.parse(root);
        FAIL();
    }
    catch(runtime_error&){}
}
TEST(RootDeviceAbstractionParserTest,notAnArray){
    cJSON* root=cJSON_CreateObject();
    cJSON_AddStringToObject(root,"type","A");
    
    RootDeviceAbstractionParser subject;
    try{
        subject.parse(root);
        FAIL();
    }
    catch(runtime_error&){}
}


