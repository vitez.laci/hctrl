#include <gtest/gtest.h>
#include "models/CWT_MB308K_OutputPort.h"
#include "MockCWT_MB308K.h"


using namespace std;
using namespace testing;

TEST(CWT_MB308K_OutputPort, PassesCallToDevice){
    MockCWT_MB308K device;
    CWT_MB308K_OutputPort subject(&device,125);
    EXPECT_CALL(device,writeOutput(125,true)).Times(1);
    subject.write(true);
    EXPECT_CALL(device,writeOutput(125,false)).Times(1);
    subject.write(false);
}

