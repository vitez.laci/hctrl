
#include <gtest/gtest.h>
#include <framework/pump.h>
#include <MockOutputPort.h>

TEST(pump, Start){
    MockOutputPort<bool> output;
    Pump subject(output);
    EXPECT_CALL(output,write(true)).Times(1);
    subject.Start();
}
TEST(pump, Stop){
    MockOutputPort<bool> output;
    Pump subject(output);
    EXPECT_CALL(output,write(false)).Times(1);
    subject.Stop();
}
