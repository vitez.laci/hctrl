#include <cstdio>
#include <iostream>
#include "lvglWrapper.h"
#include <modbus/modbus.h>
#include <thread>
#include <chrono>
#include <logManager.h>
#include <sstream>
#include <logger/item/logFactories.h>
#include <settings/HCTRSettings.h>
#include "resourceManager.h"
#include "models/BAC_1000ALN.h"

using namespace std;
using namespace LogFactories;
void doMainTask();
int main(int argc, char** argv){
    try{
        LogManager::getCoutlogger().storeLogEvent(LogEventFactory::CreateLogEvent("main","info","software started"));
        HCTRSettings::InitializeSystem("/home/laci/MyWorks/hctr/sw/settings.json");
        LogManager::getCoutlogger().storeLogEvent(LogEventFactory::CreateLogEvent("main","info","settings parsed"));
        LogManager::getCoutlogger().storeLogEvent(LogEventFactory::CreateLogEvent("main","info","main loop started"));
        while(true){
            auto start=chrono::steady_clock().now();
            doMainTask();
            auto executionDuration=chrono::steady_clock().now()-start;
            auto sleepDuration=ResourceManager::I().getMainLoopPeriod()-executionDuration;
            stringstream ss;
            ss<<"execution took "<<chrono::duration_cast<chrono::milliseconds>(executionDuration).count()<<"ms. going to sleep for "<<
            chrono::duration_cast<chrono::milliseconds>(sleepDuration).count()<<"ms.";
            LogManager::getCoutlogger().storeLogEvent(LogEventFactory::CreateLogEvent("main","info",ss.str().c_str()));
            if(sleepDuration>0ms){
                this_thread::sleep_for(sleepDuration);
            }
            else{
                ss.str("");
                ss<<"Execution take longer than main loop duration.";
                LogManager::getCoutlogger().storeLogEvent(LogEventFactory::CreateLogEvent("main","warning",ss.str().c_str()));
            }
            
        }
    }
    catch(exception& e){
        stringstream ss;
        ss<<"General exception has happened: "<<e.what();
        LogManager::getCoutlogger().storeLogEvent(LogEventFactory::CreateLogEvent("main","error",ss.str().c_str()));
    }
    this_thread::sleep_for(1s);
}
void doMainTask()
{
    ((BAC_1000ALN*)ResourceManager::I().getThermostat("nappali").get())->updateCache();
//     ResourceManager::I().updateModbusDeviceCaches();
    cout<<"nappali: "<<ResourceManager::I().getThermostat("nappali")->getTemperaturePoint()<<endl;
    cout<<"halo: "<<ResourceManager::I().getThermostat("halo")->getTemperaturePoint()<<endl;
}

